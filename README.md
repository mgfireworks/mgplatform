本系统为烟花分销平台，包括pc商管理后台、手机端店铺和手机端代理商后台     
分销+店铺模式，和传统分销软件类似有几级代理商，最终由最底层的代理商开设网上店铺，用户通过手机端访问店铺进行购买。    
系统刚搭建好，主要是由spring系列框架构成（springboot、springmvc)、模板引擎Rythm、vuejs、手机端（待定）、pc端bootstrap。  
（别问我为什么没有持久层框架，因为是自己整的-_-!）   
基础开发框架（[summer](https://git.oschina.net/xiwa/summer.git)）   
