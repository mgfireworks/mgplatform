package com.mgfireworks.mgplatform.comm;

import com.mgfireworks.mgplatform.mgadmin.user.model.RoleFunc;

import java.util.List;

/**
 * Created by xw2sy on 2017-04-02.
 */
public class StringUtils extends cn.hn.java.summer.utils.StringUtils{

    /**
     * 修正路径中的分隔符
     * @param path
     * @return
     */
    public static String repairPath(String path){
        if(path==null){
            return path;
        }
        return path.replace("\\","/").replaceAll("/{2,}","/");
    }

    /**
     *  对比两个集合
     * @param list1 理论大集合
     * @param list2 理论子集合
     * @param <T>
     * @return
     */
    public static  List<RoleFunc>  checkList(List<RoleFunc> list1,List<RoleFunc> list2) {
        if(list1.size()>=list2.size()){
            for(RoleFunc t1 : list1) {
                for(RoleFunc t2 : list2) {
                    if(t1.getFunctionId()==t2.getFunctionId()){
                        //末级才勾选，父级就先不管了
                        if(t1.getFunctionType()==2){
                            t1.setChecked(true);
                        }
                    }
                }
            }
            return list1;
        }
        return null;
    }
}
