package com.mgfireworks.mgplatform;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TaskExcuce extends BaseDao<DataSource1>{

	/**
	 * 数据库心跳
	 */
	@Scheduled(cron = "0 0/1 * * * ?")
	public void dbHeartBeat() {
		get("select 1",Integer.class);
	}
}
