package com.mgfireworks.mgplatform.mgadmin.product;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;

import com.mgfireworks.mgplatform.mgadmin.product.model.ProductDetail;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 产品详情业务类
 */
@Service
public class ProductDetailService extends BaseDao<DataSource1>{
	
	/**
	 * 取产品详情列表
	 * @return
	 */
	public List<ProductDetail> getProductDetails(ProductDetail p){
		return list("getProductDetails",ProductDetail.class,p);
	}

	/**
	 * 添加产品详情
	 */
	public void addProductDetail(ProductDetail p){
		update("addProductDetail",p);
	}
	
	/**
	 * 修改产品详情
	 */
	public void editProductDetail(ProductDetail p){
		update("editProductDetail",p);
	}
	
	/**
	 * 删除产品详情
	 */
	public void delProductDetail(ProductDetail p){
		update("delProductDetail",p);
	}
	
	/**
	 * 取产品详情
	 */
	public ProductDetail getProductDetail(ProductDetail p){
		return get("getProductDetail",ProductDetail.class,p);
	}
	
}