package com.mgfireworks.mgplatform.mgadmin.sys;

import com.mgfireworks.mgplatform.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import cn.hn.java.summer.exception.BusinessException;
import cn.hn.java.summer.mvc.Result;

@Controller
@RequestMapping("/sys/file")
public class FileController extends BaseController{
	
	@Autowired
	FileService fileService;

	/**
	 * 上传图片
	 * @param file
	 * @throws BusinessException
	 */
	@PostMapping("/uploadImg")
	public Result uploadProductMainImg(MultipartFile file,int t) throws BusinessException{
		String imgPath="";
		if(t==0 || t==1){
			imgPath=fileService.uploadProductImg(file,t);
		}else if(t==2){
			imgPath=fileService.uploadProductCateImg(file);
		}
		return new Result(true,imgPath);
	}
}
