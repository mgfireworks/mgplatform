package com.mgfireworks.mgplatform.mgadmin.sys;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import com.mgfireworks.mgplatform.mgadmin.comm.IncreaseIdService;
import com.mgfireworks.mgplatform.mgadmin.constants.OrgType;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Organization;
import com.mgfireworks.mgplatform.mgadmin.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 机构业务类
 */
@Service
public class OrganizationService extends BaseDao<DataSource1>{
	
	@Autowired
	IncreaseIdService increaseIdService;
	
	/**
	 * 取机构列表
	 * @return
	 */
	public List<Organization> getOrganizations(Organization p){
		return list("getOrganizations",Organization.class,p);
	}

	/**
	 * 取所有厂家
	 * @return
	 */
	public List<Organization> getAllFactories(){
		return all(
			"getOrganizations",
			Organization.class,
			new Object[][]{{"orgType",OrgType.FACTORY.value()}}
		);
	}

	/**
	 * 添加机构
	 */
	public void addOrganization(Organization p){
		update("addOrganization",p);
	}
	
	/**
	 * 修改机构
	 */
	public void editOrganization(Organization p){
		update("editOrganization",p);
	}
	
	/**
	 * 删除机构
	 */
	public void delOrganization(Organization p){
		update("delOrganization",p);
	}
	
	/**
	 * 取机构
	 */
	@Cacheable(value = "orgs", key = "'getOrganizationById'+#orgId")
	public Organization getOrganizationById(int orgId){
		return get("getOrganization",Organization.class,ce().set("orgId",orgId));
	}

	/**
	 * 取机构
	 */
	public Organization getOrganization(Organization p){
		return get("getOrganization",Organization.class,p);
	}
	
	/**
	 * 获取一个新机构
	 * @return
	 */
	public Organization newOrganization(){
		return new Organization(increaseIdService.getNewOrganizationId());
	}


	///////////////////////代理商/////////////////////////

	/**
	 * 取一级代理用户的下级代理商列表
	 * @return
	 */
	public List<Organization> getSubAgents(User user){
		return list("getSubAgents",Organization.class,user);
	}
}