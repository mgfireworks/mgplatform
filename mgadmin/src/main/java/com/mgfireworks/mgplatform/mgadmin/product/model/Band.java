package com.mgfireworks.mgplatform.mgadmin.product.model;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class Band {
	/**
	 * 品牌编号 
	 */
 	@NotNull(message="品牌编号不能为空")
	private Integer bandId;
	
	/**
	 * 品牌名称 
	 */
 	@NotNull(message="品牌名称不能为空")
	private String bandName;
	
	/**
	 * 标识位 
	 */
 	@NotNull(message="标识位不能为空")
	private Integer flag;

	public Band() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Band(Integer bandId, String bandName, Integer flag) {
		super();
		this.bandId = bandId;
		this.bandName = bandName;
		this.flag = flag;
	}

	public Band(Integer bandId, String bandName) {
		super();
		this.bandId = bandId;
		this.bandName = bandName;
	}

	public Band(Integer bandId) {
		super();
		this.bandId = bandId;
	}
 	
}