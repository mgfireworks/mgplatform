package com.mgfireworks.mgplatform.mgadmin.product;

import cn.hn.java.summer.annotation.ParamValid;
import com.mgfireworks.mgplatform.mgadmin.comm.IncreaseIdService;
import com.mgfireworks.mgplatform.mgadmin.product.model.Band;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 
 * @author nap
 * 20170225-12:46:26
 * 品牌控制器
 */
@Controller
@RequestMapping("/product/band")
public class BandController {
	
	@Autowired
	IncreaseIdService increaseIdService;

	@Autowired
	BandService bandService;
	
	/**
	 * 品牌列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<Band> list(Band p){
		return bandService.getBands(p);
	}

	/**
	 * 添加品牌
	 */
	@GetMapping("/add")
	public Band add(){
		return new Band(increaseIdService.getNewBandId());
	}
	
	/**
	 * 提交添加品牌
	 */
	@PostMapping("/add")
	public void postAdd(@ParamValid Band p){
		bandService.addBand(p);
	}

	/**
	 * 编辑品牌
	 */
	@GetMapping("/edit")
	public Band edit(Band p){
		return bandService.getBand(p);
	}
	
	/**
	 * 提交编辑品牌
	 */
	@PostMapping("/edit")
	public void postEdit(@ParamValid Band p){
		bandService.editBand(p);
	}
	
	/**
	 * 获取品牌
	 */
	@GetMapping("/get")
	public Band get(Band p){
		return bandService.getBand(p);
	}
	
	/**
	 * 删除品牌
	 */
	@PostMapping("/del")
	public void del(Band p){
		bandService.delBand(p);
	}
}