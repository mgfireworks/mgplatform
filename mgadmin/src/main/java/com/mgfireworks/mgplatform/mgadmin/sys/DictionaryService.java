package com.mgfireworks.mgplatform.mgadmin.sys;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mgfireworks.mgplatform.mgadmin.comm.CacheService;
import com.mgfireworks.mgplatform.mgadmin.comm.IncreaseIdService;
import com.mgfireworks.mgplatform.mgadmin.constants.Constant;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Dictionary;
import com.mgfireworks.mgplatform.mgadmin.sys.model.SellLevel;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 字典表业务类
 */
@Service
public class DictionaryService extends BaseDao<DataSource1>{
	@Autowired
	IncreaseIdService increaseIdService;
	@Autowired
	CacheService cacheService;
	
	/**
	 * 取字典表列表
	 * @return
	 */
	public List<Dictionary> getDictionarys(Dictionary p){
		return list("getDictionarys",Dictionary.class,p);
	}

	/**
	 * 添加字典表
	 */
	public void addDictionary(Dictionary p){
		update("addDictionary",p);
	}
	
	/**
	 * 修改字典表
	 */
	public void editDictionary(Dictionary p){
		update("editDictionary",p);
	}
	
	/**
	 * 删除字典表
	 */
	public void delDictionary(Dictionary p){
		update("delDictionary",p);
	}
	
	/**
	 * 取字典表
	 */
	public Dictionary getDictionary(Dictionary p){
		return get("getDictionary",Dictionary.class,p);
	}
	
	/**
	 * 取新ID字典
	 * @return
	 */
	public Dictionary newDictionaryId(){
		Dictionary dic=new Dictionary();
		dic.setDicId(increaseIdService.getNewDictionaryId());
		return dic;
	}
	
	
	/**
	 * 取所有销售级别
	 * @return
	 */
	public List<SellLevel> getAllSellLevels(){
		List<Dictionary> allDic= cacheService.getAllDictionaries();
		List<SellLevel> dicLevels=allDic.stream()
			.filter(d->d.getParentId()==Constant.SELLlEVEL_PARENTID)
			.map(d->new SellLevel(Integer.parseInt(d.getDicValue()),d.getDicName()))
			.collect(Collectors.toList());
		return dicLevels;
	}
}