package com.mgfireworks.mgplatform.mgadmin.stock.model;

import lombok.Data;

import java.util.ArrayList;

/**
 * Created by xw2sy on 2017-04-09.
 */
@Data
public class OrderItemModel {
    private ArrayList<OrderItem> items;
}
