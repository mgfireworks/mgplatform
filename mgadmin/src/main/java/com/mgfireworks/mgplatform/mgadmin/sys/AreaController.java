package com.mgfireworks.mgplatform.mgadmin.sys;

import cn.hn.java.summer.annotation.ParamValid;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Area;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 区域控制器
 */
@Controller
@RequestMapping("/sys/area")
public class AreaController {

	@Autowired
	AreaService areaService;
	
	/**
	 * 区域列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<Area> list(Area p){
		return areaService.getAreas(p);
	}

	/**
	 * 区域选择列表
	 * @return
	 */
	@RequestMapping("/select")
	public List<Area> listSelect(Area p){
		return areaService.getAreas(p);
	}

	/**
	 * 添加区域
	 */
	@GetMapping("/add")
	public void add(){
	}
	
	/**
	 * 提交添加区域
	 */
	@PostMapping("/add")
	public void postAdd(@ParamValid Area p){
		areaService.addArea(p);
	}

	/**
	 * 编辑区域
	 */
	@GetMapping("/edit")
	public Area edit(Area p){
		return areaService.getArea(p);
	}
	
	/**
	 * 提交编辑区域
	 */
	@PostMapping("/edit")
	public void postEdit(@ParamValid Area p){
		areaService.editArea(p);
	}
	
	/**
	 * 获取区域
	 */
	@GetMapping("/get")
	public Area get(Area p){
		return areaService.getArea(p);
	}
	
	/**
	 * 删除区域
	 */
	@PostMapping("/del")
	public void del(Area p){
		areaService.delArea(p);
	}
}