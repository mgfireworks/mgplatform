package com.mgfireworks.mgplatform.mgadmin.user.model;

import lombok.Data;

import java.util.Set;
/**
 * Created by af on 2017/4/23.
 */
@Data
public class RoleFunc {
    private Integer functionId;

    private String functionName;

    private String functionUrl;

    private Integer parentId;

    private Integer functionType;

    private boolean isChecked;

    private Set<RoleFunc> nodes;

    public RoleFunc(Integer functionId, String functionName, String functionUrl, Integer parentId, Integer functionType, Set<RoleFunc> nodes) {
        this.functionId = functionId;
        this.functionName = functionName;
        this.functionUrl = functionUrl;
        this.parentId = parentId;
        this.functionType = functionType;
        this.nodes = nodes;
    }

    public RoleFunc(Integer functionId, String functionName, String functionUrl, Integer parentId, Integer functionType) {
        this.functionId = functionId;
        this.functionName = functionName;
        this.functionUrl = functionUrl;
        this.parentId = parentId;
        this.functionType = functionType;
    }

    public RoleFunc(){

    }
}
