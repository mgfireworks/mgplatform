package com.mgfireworks.mgplatform.mgadmin.stock.model;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by xw2sy on 2017-03-25.
 */
@Data
public class BatchStockInit {

    /**
     * 多个产品id
     */
    @NotNull(message = "必须选择产品")
    @Size(min = 1,message = "至少选择1个产品")
    private List<String> productId;

    /**
     * 数量
     */
    @NotNull(message = "数量不能为空")
    @Min(message = "数量必须大于0",value = 1)
    private Integer quantity;

    /**
     * 价格
     */
    @NotNull(message = "价格不能为空")
    @Min(message = "价格必须大于0",value = 1)
    private Double price;
}
