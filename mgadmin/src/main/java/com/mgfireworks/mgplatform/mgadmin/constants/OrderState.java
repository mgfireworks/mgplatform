package com.mgfireworks.mgplatform.mgadmin.constants;

/**
 * 订单类型
 */
public enum OrderState {
	UNPAID(0,"未支付"),
	PAID(1,"已支付"),
	REFUNDED(-1,"已退款"),
	SHIPPED(2,"已发货"),
	CANCEL_SHIPPED(-2,"取消发货"),
	RECEIVED (3,"已收货"),
	RETURNED(-3,"已退货"),
	FINISHED(4,"已完成"),
	CLOSED(-4,"已关闭");


	private final int value;

	private final String phrase;

	OrderState(int value, String phrase) {
		this.value = value;
		this.phrase = phrase;
	}
	/**
	 * @return 整型编码
	 */
	public int value() {
		return this.value;
	}

	/**
	 * @return 描述
	 */
	public String getPhrase() {
		return this.phrase;
	}


	public static OrderState valueOf(int v) {
		for (OrderState val : values()) {
			if (val.value == v) {
				return val;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return phrase;
	}
	
}
