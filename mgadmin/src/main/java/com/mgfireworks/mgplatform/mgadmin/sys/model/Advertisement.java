package com.mgfireworks.mgplatform.mgadmin.sys.model;
import java.util.Date;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Advertisement {
	/**
	 * 广告编号 
	 */
 	@NotNull(message="广告编号不能为空")
	private Integer advId;
	
	/**
	 * 链接地址 点击广告跳转的地址
	 */
 	@NotNull(message="链接地址不能为空")
	private String linkUrl;
	
	/**
	 * 广告类型 0:图片,1:视频
	 */
 	@NotNull(message="广告类型不能为空")
	private Integer advType;
	
	/**
	 * 广告资源 图片、视频存放路径
	 */
 	@NotNull(message="广告资源不能为空")
	private String advResource;
	
	/**
	 * 播放地区 不同地区播放不同广告
	 */

	private Integer areaId;
	
	/**
	 * 是否上线 终端只显示上线的广告
	 */
 	@NotNull(message="是否上线不能为空")
	private Integer online;
	
	/**
	 * 添加时间 
	 */

	private Date addTime;
	
}