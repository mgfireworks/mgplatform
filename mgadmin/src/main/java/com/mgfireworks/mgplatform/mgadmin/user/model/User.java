package com.mgfireworks.mgplatform.mgadmin.user.model;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Organization;
import com.mgfireworks.mgplatform.mgadmin.sys.valid.UserEdit;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class User {
	/**
	 * 用户编号 
	 */
 	@NotNull(message="用户编号不能为空",groups = UserEdit.class)
	private Integer userId;
	
	/**
	 * 用户账号 
	 */
 	@NotNull(message="用户账号不能为空")
	private String userAccount;
	
	/**
	 * 用户名称 
	 */
 	@NotNull(message="用户名称不能为空")
	private String userName;
	
	/**
	 * 用户密码 
	 */
 	@NotNull(message="用户密码不能为空")
	private String userPwd;
	
	/**
	 * 所属机构 
	 */
 	@NotNull(message="所属机构不能为空")
	private Integer orgId;
 	
 	/**
 	 * 机构名
 	 */
 	private String orgName;
	
	/**
	 * 用户角色 
	 */
 	@NotNull(message="用户角色不能为空")
	private Integer userRole;

	/**
	 * 角色名
	 */
	private String roleName;
	
	/**
	 * 最后登录时间 
	 */

	private Long lastLoginTime;
	
	/**
	 * 最后登录IP 
	 */

	private String lastLoginIp;

	/**
	 * 区域编号
	 */
	private String areaId;

	private Organization org;

	public User(Integer userId) {
		super();
		this.userId = userId;
	}

	public User(String userAccount) {
		super();
		this.userAccount = userAccount;
	}

	public User() {
		super();
	}
	
}