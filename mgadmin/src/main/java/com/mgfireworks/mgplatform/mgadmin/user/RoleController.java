package com.mgfireworks.mgplatform.mgadmin.user;

import cn.hn.java.summer.annotation.ParamValid;
import com.mgfireworks.mgplatform.mgadmin.user.model.Role;
import com.mgfireworks.mgplatform.mgadmin.user.model.RoleFunc;
import org.rythmengine.utils.F;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 用户角色控制器
 */
@Controller
@RequestMapping("/user/role")
public class RoleController {

	@Autowired
	RoleService roleService;
	
	/**
	 * 用户角色列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<Role> list(Role p){
		return roleService.getRoles(p);
	}

	/**
	 * 用户角色选择列表
	 * @return
	 */
	@RequestMapping("/select")
	public List<Role> selectList(Role p){
		return roleService.getRoles(p);
	}

	/**
	 * 添加用户角色
	 */
	@GetMapping("/add")
	public void add(){
	}
	
	/**
	 * 提交添加用户角色
	 */
	@PostMapping("/add")
	public void postAdd(@ParamValid Role p){
		roleService.addRole(p);
	}

//	@GetMapping("/edit")
//	public Role edit(Role p){
//		return roleService.getRole(p);
//	}

	/**
	 * 编辑用户角色
	 */
	@GetMapping("/edit")
	public F.T2<Role ,List<RoleFunc>> edit(Integer roleId){
		return F.T2(roleService.getRole(roleId),roleService.getRoleFunc(roleId));
	}

//	@GetMapping("/edit")
//	public List<RoleFunc> edit(Role p){
//		return roleService.getRoleFunc();
//	}
	
	/**
	 * 提交编辑用户角色
	 */
	@PostMapping("/edit")
	public void postEdit(@ParamValid Role p){
		roleService.editRole(p);
	}
	
	/**
	 * 获取用户角色
	 */
	@GetMapping("/get")
	public Role get(Role p){
		return roleService.getRole(p);
	}

	/**
	 * 删除用户角色
	 */
	@PostMapping("/del")
	public void del(Role p){
		roleService.delRole(p);
	}

}