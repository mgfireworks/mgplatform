package com.mgfireworks.mgplatform.mgadmin.comm;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.mgfireworks.mgplatform.mgadmin.sys.model.Dictionary;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;

@Service
public class CacheService extends BaseDao<DataSource1>{

	@Cacheable("dictionaries")
	public List<Dictionary> getAllDictionaries(){
		return all("getDictionarys", Dictionary.class);
	}
}
