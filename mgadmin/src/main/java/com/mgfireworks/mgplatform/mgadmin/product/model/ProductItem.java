package com.mgfireworks.mgplatform.mgadmin.product.model;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ProductItem {
	/**
	 * 产品项编号 
	 */
 	@NotNull(message="产品项编号不能为空")
	private Integer itemId;
	
	/**
	 * 项目类别 
	 */
 	@NotNull(message="项目类别不能为空")
	private Integer itemType;
	
	/**
	 * 项目名称 
	 */
 	@NotNull(message="项目名称不能为空")
	private String itemName;
	
	/**
	 * 产品编号 
	 */
 	@NotNull(message="产品编号不能为空")
	private String productId;
	
}