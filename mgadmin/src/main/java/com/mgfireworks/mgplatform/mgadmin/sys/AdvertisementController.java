package com.mgfireworks.mgplatform.mgadmin.sys;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.hn.java.summer.annotation.ParamValid;

import com.mgfireworks.mgplatform.mgadmin.sys.model.Advertisement;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 广告控制器
 */
@Controller
@RequestMapping("/sys/advertisement")
public class AdvertisementController {

	@Autowired
	AdvertisementService advertisementService;
	
	/**
	 * 广告列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<Advertisement> list(Advertisement p){
		return advertisementService.getAdvertisements(p);
	}

	/**
	 * 添加广告
	 */
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public void add(){
	}
	
	/**
	 * 提交添加广告
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public void postAdd(@ParamValid Advertisement p){
		advertisementService.addAdvertisement(p);
	}

	/**
	 * 编辑广告
	 */
	@RequestMapping(value="/edit",method=RequestMethod.GET)
	public Advertisement edit(Advertisement p){
		return advertisementService.getAdvertisement(p);
	}
	
	/**
	 * 提交编辑广告
	 */
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public void postEdit(@ParamValid Advertisement p){
		advertisementService.editAdvertisement(p);
	}
	
	/**
	 * 获取广告
	 */
	@RequestMapping(value="/get",method=RequestMethod.GET)
	public Advertisement get(Advertisement p){
		return advertisementService.getAdvertisement(p);
	}
	
	/**
	 * 删除广告
	 */
	@RequestMapping("/del")
	public void del(Advertisement p){
		advertisementService.delAdvertisement(p);
	}
}