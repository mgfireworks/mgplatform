package com.mgfireworks.mgplatform.mgadmin.sys.model;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Function {
	/**
	 * 功能编号 
	 */
 	@NotNull(message="功能编号不能为空")
	private String functionId;
	
	/**
	 * 功能名称 
	 */
 	@NotNull(message="功能名称不能为空")
	private String functionName;
	
	/**
	 * 访问地址 
	 */

	private String functionUrl;
	
	/**
	 * 功能类别 0:普通功能,1:显示在菜单
	 */
 	@NotNull(message="功能类别不能为空")
	private Integer functionType;
	
	/**
	 * 父级编号 
	 */
	private String parentId;
	
}