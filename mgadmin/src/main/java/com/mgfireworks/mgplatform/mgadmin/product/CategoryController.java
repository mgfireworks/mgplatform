package com.mgfireworks.mgplatform.mgadmin.product;

import cn.hn.java.summer.annotation.ParamValid;
import com.mgfireworks.mgplatform.mgadmin.product.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 产品类别控制器
 */
@Controller
@RequestMapping("/product/category")
public class CategoryController {

	@Autowired
	CategoryService categoryService;
	
	/**
	 * 产品类别列表
	 * @return
	 */
	@RequestMapping(value = "/list")
	public List<Category> list(Category p){
		return categoryService.getProductCategorys(p);
	}

	/**
	 * 添加产品类别
	 */
	@GetMapping("/add")
	public Category add(int parentId){
		return categoryService.newCategory(parentId);
	}
	
	/**
	 * 提交添加产品类别
	 */
	@PostMapping("/add")
	public void postAdd(@ParamValid Category p){
		categoryService.addProductCategory(p);
	}

	/**
	 * 编辑产品类别
	 */
	@GetMapping("/edit")
	public Category edit(Category p){
		return categoryService.getProductCategory(p);
	}
	
	/**
	 * 提交编辑产品类别
	 */
	@PostMapping("/edit")
	public void postEdit(@ParamValid Category p){
		categoryService.editProductCategory(p);
	}
	
	/**
	 * 获取产品类别
	 */
	@GetMapping("/get")
	public Category get(Category p){
		return categoryService.getProductCategory(p);
	}
	
	/**
	 * 删除产品类别
	 */
	@PostMapping("/del")
	public void del(Category p){
		categoryService.delProductCategory(p);
	}
}