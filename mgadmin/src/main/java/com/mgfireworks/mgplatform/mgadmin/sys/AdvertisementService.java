package com.mgfireworks.mgplatform.mgadmin.sys;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;

import com.mgfireworks.mgplatform.mgadmin.sys.model.Advertisement;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 广告业务类
 */
@Service
public class AdvertisementService extends BaseDao<DataSource1>{
	
	/**
	 * 取广告列表
	 * @return
	 */
	public List<Advertisement> getAdvertisements(Advertisement p){
		return list("getAdvertisements",Advertisement.class,p);
	}

	/**
	 * 添加广告
	 */
	public void addAdvertisement(Advertisement p){
		update("addAdvertisement",p);
	}
	
	/**
	 * 修改广告
	 */
	public void editAdvertisement(Advertisement p){
		update("editAdvertisement",p);
	}
	
	/**
	 * 删除广告
	 */
	public void delAdvertisement(Advertisement p){
		update("delAdvertisement",p);
	}
	
	/**
	 * 取广告
	 */
	public Advertisement getAdvertisement(Advertisement p){
		return get("getAdvertisement",Advertisement.class,p);
	}
	
}