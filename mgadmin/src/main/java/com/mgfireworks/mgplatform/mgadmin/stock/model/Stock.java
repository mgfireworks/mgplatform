package com.mgfireworks.mgplatform.mgadmin.stock.model;

import com.mgfireworks.mgplatform.mgadmin.product.model.Product;
import com.mgfireworks.mgplatform.mgadmin.stock.valid.StockEdit;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Stock extends Product implements Cloneable{
    /**
     * 库存编号(14位)
     * 4位机构编号+yyMMdd+4位序号
     */
    @NotNull(message = "库存编号不能为空",groups = StockEdit.class)
    private String stockId;

    /**
     * 下一个库存编号
     * 当前库存下架后再上架会创建一个新的库存记录，新的库存编号会记录在原库存的nextStockId中
     * 展示库存时只显示prevStockId为空的记录（即不包含原被下架的最新记录）
     */
    private String nextStockId;

    /**
     * 所属机构编号
     */
    //@NotNull(message = "所属机构编号不能为空")
    private Integer orgId;

    private String orgName;//机构名称

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 区域编号
     */
    private String areaId;

    private String areaName;//区域名称

    /**
     * 库存数
     */
    @NotNull(message = "库存数不能为空",groups = StockEdit.class)
    private Integer quantity;

    /**
     * 价格
     */
    @NotNull(message = "价格不能为空",groups = StockEdit.class)
    private Double price;

    /**
     * 是否上线 0:未上线,1:上线
     */
    @NotNull(message = "是否上线不能为空",groups = StockEdit.class)
    private Integer online;

    /**
     * 是否缺货 0:缺,1:不缺
     */
    //@NotNull(message = "是否缺货不能为空")
    private Integer outOfStock;

    /**
     * 审核状态(0:未审核,1:审核)
     */
    private Integer checkState;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}