package com.mgfireworks.mgplatform.mgadmin.user;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import com.mgfireworks.mgplatform.mgadmin.comm.IncreaseIdService;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Organization;
import com.mgfireworks.mgplatform.mgadmin.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 代理商管理
 */
@Service
public class AgentService extends BaseDao<DataSource1>{
	@Autowired
	IncreaseIdService increaseIdService;
	
	/**
	 * 取下级代理商列表
	 * @return
	 */
	public List<Organization> getSubAgents(User user){
		return list("getSubAgents",Organization.class,new Organization(user.getOrgId()));
	}
}