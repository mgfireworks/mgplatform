package com.mgfireworks.mgplatform.mgadmin.user.model;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Role {
	/**
	 * 角色编号 
	 */
 	@NotNull(message="角色编号不能为空")
	private Integer roleId;
	
	/**
	 * 角色名称 
	 */
 	@NotNull(message="角色名称不能为空")
	private String roleName;
	
}