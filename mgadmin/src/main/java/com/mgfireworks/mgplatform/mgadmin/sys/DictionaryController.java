package com.mgfireworks.mgplatform.mgadmin.sys;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.hn.java.summer.annotation.ParamValid;

import com.mgfireworks.mgplatform.mgadmin.constants.Constant;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Dictionary;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 字典表控制器
 */
@Controller
@RequestMapping("/sys/dictionary")
public class DictionaryController {

	@Autowired
	DictionaryService dictionaryService;
	
	/**
	 * 字典表列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<Dictionary> list(Dictionary p){
		return dictionaryService.getDictionarys(p);
	}

	/**
	 * 添加字典表
	 */
	@GetMapping("/add")
	public void add(){
	}
	
	/**
	 * 提交添加字典表
	 */
	@PostMapping("/add")
	public void postAdd(@ParamValid Dictionary p){
		dictionaryService.addDictionary(p);
	}

	/**
	 * 编辑字典表
	 */
	@GetMapping("/edit")
	public Dictionary edit(Dictionary p){
		return dictionaryService.getDictionary(p);
	}
	
	/**
	 * 提交编辑字典表
	 */
	@PostMapping("/edit")
	public void postEdit(@ParamValid Dictionary p){
		dictionaryService.editDictionary(p);
	}
	
	/**
	 * 获取字典表
	 */
	@GetMapping("/get")
	public Dictionary get(Dictionary p){
		return dictionaryService.getDictionary(p);
	}
	
	/**
	 * 删除字典表
	 */
	@PostMapping("/del")
	public void del(Dictionary p){
		dictionaryService.delDictionary(p);
	}
	
	/**
	 * 销售级别列表
	 * @return
	 */
	@RequestMapping("/sellLevelList")
	public List<Dictionary> sellLevelList(Dictionary p){
		p.setParentId(Constant.SELLlEVEL_PARENTID);
		return dictionaryService.getDictionarys(p);
	}
	/**
	 * 编辑销售级别
	 */
	@GetMapping("/editSellLevel")
	public Dictionary editSellLevel(Dictionary p){
		return dictionaryService.getDictionary(p);
	}
	/**
	 * 提交编辑销售级别
	 */
	@PostMapping("/editSellLevel")
	public void postEditSellLevel(@ParamValid Dictionary p){
		p.setParentId(Constant.SELLlEVEL_PARENTID);
		dictionaryService.editDictionary(p);
	}
	/**
	 * 提交添加销售级别
	 */
	@GetMapping("/addSellLevel")
	public Dictionary addSellLevel(){
		return dictionaryService.newDictionaryId();
	}
	/**
	 * 提交添加销售级别
	 */
	@PostMapping("/addSellLevel")
	public void postAddSellLevel(@ParamValid Dictionary p){
		p.setParentId(Constant.SELLlEVEL_PARENTID);
		dictionaryService.addDictionary(p);
	}
	/**
	 * 删除字典表
	 */
	@RequestMapping("/delSellLevel")
	public void delSellLevel(Dictionary p){
		dictionaryService.delDictionary(p);
	}
}