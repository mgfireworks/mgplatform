package com.mgfireworks.mgplatform.mgadmin.stock.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by af on 2017/3/26.
 */

@Data
public class Order {
    /**
     * 订单ID(4位orgId+yyMMdd+4位序号=14位)
     */
    private String orderId;

    /**
     * 采购代理商编号
     */
    private Integer orgId;

    /**
     * 采购用户编号
     */
    private Integer userId;

    /**
     * 订单项数量
     */
    private Integer itemCount;

    /**
     * 订单金额
     */
    private Double orderAmount;

    /**
     * 订单时间
     */
    private Date orderTime;

    /**
     * 订单状态(0:未支付,1:已支付(-1:已退款),2:已发货(-2:取消发货),3:已收货(-3:已退货),4:已完成(-4:已关闭)
     */
    private Integer orderState;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 操作者
     */
    private Integer operateId;

    /**
     * 订单项信息列表
     */
    private List<OrderItem> orderItemInfoList;
}
