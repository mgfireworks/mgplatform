package com.mgfireworks.mgplatform.mgadmin.sys.model;

import lombok.Data;

/**
 * 销售级别
 * @author sjg
 * 2017年2月19日 下午7:34:47
 *
 */
@Data
public class SellLevel {

	/**
	 * 级别
	 */
	private Integer level;
	
	/**
	 * 级别名
	 */
	private String levelName;

	public SellLevel() {
	}

	public SellLevel(Integer level, String levelName) {
		this.level = level;
		this.levelName = levelName;
	}

}
