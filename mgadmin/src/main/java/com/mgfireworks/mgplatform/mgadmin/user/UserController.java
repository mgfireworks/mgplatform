package com.mgfireworks.mgplatform.mgadmin.user;

import cn.hn.java.summer.annotation.ParamValid;
import com.mgfireworks.mgplatform.mgadmin.sys.valid.UserAdd;
import com.mgfireworks.mgplatform.mgadmin.user.model.Role;
import com.mgfireworks.mgplatform.mgadmin.user.model.User;
import org.rythmengine.utils.F;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 用户控制器
 */
@Controller
@RequestMapping("/user/user")
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	RoleService roleService;
	
	/**
	 * 用户列表
	 * @return
	 */
	@RequestMapping("/list")
	public F.T2<List<User>,List<Role>> list(User p){
		return F.T2(userService.getUsers(p),roleService.getAllRoles());
	}

	/**
	 * 添加用户
	 */
	@GetMapping("/add")
	public void add(User user){
	}
	
	/**
	 * 提交添加用户
	 */
	@PostMapping("/add")
	public void postAdd(@ParamValid(value = UserAdd.class) User p){
		userService.addUser(p);
	}

	/**
	 * 编辑用户
	 */
	@GetMapping("/edit")
	public User edit(User p){
		return userService.getUser(p);
	}
	
	/**
	 * 提交编辑用户
	 */
	@PostMapping("/edit")
	public void postEdit(@ParamValid User p){
		userService.editUser(p);
	}
	
	/**
	 * 获取用户
	 */
	@GetMapping("/get")
	public User get(User p){
		return userService.getUser(p);
	}
	
	/**
	 * 删除用户
	 */
	@PostMapping("/del")
	public void del(User p){
		userService.delUser(p);
	}
}