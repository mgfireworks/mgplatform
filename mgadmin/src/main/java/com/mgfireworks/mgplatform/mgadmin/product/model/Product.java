package com.mgfireworks.mgplatform.mgadmin.product.model;
import com.mgfireworks.mgplatform.mgadmin.product.valid.ProductEdit;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Product {
	/**
	 * 产品编号 4位cateId+4位bandId+4位序号
	 */
 	@NotNull(message="产品编号不能为空",groups = ProductEdit.class)
	private String productId;
	
	/**
	 * 产品名 
	 */
 	@NotNull(message="产品名不能为空")
	private String productName;
	
	/**
	 * 类别 
	 */
 	@NotNull(message="类别不能为空")
	private Integer cateId;
	
	/**
	 * 类别名 
	 */

	private String categoryName;
	
	/**
	 * 品牌 
	 */
 	@NotNull(message="品牌不能为空")
	private Integer bandId;
	
	/**
	 * 品牌名 
	 */

	private String bandName;
	
	/**
	 * 主图片 
	 */

	private String mainImg;

	private Integer orgId;

	/**
	 * 销售级别 对应字典项中销售级别值
	 */
 	@NotNull(message="销售级别不能为空")
	private Integer sellLevel;
	
	/**
	 * 销售级别名 
	 */

	private String sellLevelName;
	
	/**
	 * 是否推荐 0:不推荐,1:推荐
	 */
 	@NotNull(message="是否推荐不能为空")
	private Integer recommend;
	
	/**
	 * 添加时间 
	 */

	private String addTime;
	
	/**
	 * 更新时间 
	 */

	private String updateTime;


	public Product(String productId) {
		super();
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "Product{" +
				"productId='" + productId + '\'' +
				", productName='" + productName + '\'' +
				", cateId=" + cateId +
				", categoryName='" + categoryName + '\'' +
				", bandId=" + bandId +
				", bandName='" + bandName + '\'' +
				", mainImg='" + mainImg + '\'' +
				", sellLevel=" + sellLevel +
				", sellLevelName='" + sellLevelName + '\'' +
				", recommend=" + recommend +
				", addTime='" + addTime + '\'' +
				", updateTime='" + updateTime + '\'' +
				'}';
	}

	public Product() {
		super();
	}
	
}