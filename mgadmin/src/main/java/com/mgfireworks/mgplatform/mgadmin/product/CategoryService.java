package com.mgfireworks.mgplatform.mgadmin.product;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import com.mgfireworks.mgplatform.mgadmin.comm.IncreaseIdService;
import com.mgfireworks.mgplatform.mgadmin.product.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 产品类别业务类
 */
@Service
public class CategoryService extends BaseDao<DataSource1>{

	@Autowired
	IncreaseIdService increaseIdService;
	
	/**
	 * 取所有类别
	 * @return
	 */
	public List<Category> getAllCategorys(){
		return all("getProductCategorys",Category.class);
	}
	
	/**
	 * 取产品类别列表
	 * @return
	 */
	public List<Category> getProductCategorys(Category p){
		return list("getProductCategorys",Category.class,p);
	}

	/**
	 * 添加产品类别
	 */
	public void addProductCategory(Category p){
		update("addProductCategory",p);
	}
	
	/**
	 * 修改产品类别
	 */
	public void editProductCategory(Category p){
		update("editProductCategory",p);
	}
	
	/**
	 * 删除产品类别
	 */
	public void delProductCategory(Category p){
		update("delProductCategory",p);
	}
	
	/**
	 * 取产品类别
	 */
	public Category getProductCategory(Category p){
		return get("getProductCategory",Category.class,p);
	}
	
	/**
	 * 获取一个新的类别
	 * @param parentId
	 * @return
	 */
	public Category newCategory(int parentId){
		Category parent=null;
		if(parentId!=0){
			parent=getProductCategory(new Category(parentId));
		}else{
			parent=new Category(0);
			parent.setCateName("无");
		}
		Category newCate=new Category(increaseIdService.getNewCategoryId(), parentId);
		newCate.setParentName(parent.getCateName());
		return newCate;
	}
	
}