package com.mgfireworks.mgplatform.mgadmin.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.hn.java.summer.annotation.ParamValid;

import com.mgfireworks.mgplatform.mgadmin.product.model.ProductItem;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 产品项目控制器
 */
@Controller
@RequestMapping("/product/productitem")
public class ProductItemController {

	@Autowired
	ProductItemService productitemService;
	
	/**
	 * 产品项目列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<ProductItem> list(ProductItem p){
		return productitemService.getProductItems(p);
	}

	/**
	 * 添加产品项目
	 */
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public void add(){
	}
	
	/**
	 * 提交添加产品项目
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public void postAdd(@ParamValid ProductItem p){
		productitemService.addProductItem(p);
	}

	/**
	 * 编辑产品项目
	 */
	@RequestMapping(value="/edit",method=RequestMethod.GET)
	public ProductItem edit(ProductItem p){
		return productitemService.getProductItem(p);
	}
	
	/**
	 * 提交编辑产品项目
	 */
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public void postEdit(@ParamValid ProductItem p){
		productitemService.editProductItem(p);
	}
	
	/**
	 * 获取产品项目
	 */
	@RequestMapping(value="/get",method=RequestMethod.GET)
	public ProductItem get(ProductItem p){
		return productitemService.getProductItem(p);
	}
	
	/**
	 * 删除产品项目
	 */
	@RequestMapping("/del")
	public void del(ProductItem p){
		productitemService.delProductItem(p);
	}
}