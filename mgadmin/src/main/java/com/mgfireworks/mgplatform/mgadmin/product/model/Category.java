package com.mgfireworks.mgplatform.mgadmin.product.model;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Category {
	/**
	 * 类别编号 
	 */
 	@NotNull(message="类别编号不能为空")
	private Integer cateId;
	
	/**
	 * 类别名称 
	 */
 	@NotNull(message="类别名称不能为空")
	private String cateName;
	
	/**
	 * 编号路径 用于通配查询
	 */

	private String idPath;
	
	/**
	 * 标识位 
	 */

	private Integer flag;
	
	/**
	 * 父级编号 
	 */

	private Integer parentId;

	/**
	 * 分类图片
	 */
	private String cateImg;
	
	/**
	 * 父级分类名
	 */
	private String parentName;

	public Category(Integer cateId, Integer parentId) {
		super();
		this.cateId = cateId;
		this.parentId = parentId;
	}

	public Category() {
		super();
	}

	public Category(Integer cateId) {
		super();
		this.cateId = cateId;
	}
	
	
}