package com.mgfireworks.mgplatform.mgadmin.sys.model;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Dictionary {
	/**
	 * 字典编号 
	 */
 	@NotNull(message="字典编号不能为空")
	private Integer dicId;
	
	/**
	 * 字典名称 
	 */
 	@NotNull(message="字典名称不能为空")
	private String dicName;
	
	/**
	 * 字典值 
	 */
 	@NotNull(message="字典值不能为空")
	private String dicValue;
	
	/**
	 * 父级编号 
	 */

	private Integer parentId;
	
	/**
	 * 描述 
	 */

	private String description;
	
}