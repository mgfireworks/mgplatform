package com.mgfireworks.mgplatform.mgadmin.stock;

import cn.hn.java.summer.annotation.ParamValid;
import cn.hn.java.summer.exception.BusinessException;
import com.mgfireworks.mgplatform.BaseController;
import com.mgfireworks.mgplatform.mgadmin.product.CategoryService;
import com.mgfireworks.mgplatform.mgadmin.product.model.Category;
import com.mgfireworks.mgplatform.mgadmin.product.model.Product;
import com.mgfireworks.mgplatform.mgadmin.stock.model.BatchStockInit;
import com.mgfireworks.mgplatform.mgadmin.stock.model.Stock;
import com.mgfireworks.mgplatform.mgadmin.stock.model.StockDetail;
import com.mgfireworks.mgplatform.mgadmin.stock.valid.StockEdit;
import com.mgfireworks.mgplatform.mgadmin.sys.AreaService;
import com.mgfireworks.mgplatform.mgadmin.sys.DictionaryService;
import com.mgfireworks.mgplatform.mgadmin.sys.OrganizationService;
import com.mgfireworks.mgplatform.mgadmin.sys.model.SellLevel;
import com.mgfireworks.mgplatform.mgadmin.user.model.User;
import org.rythmengine.utils.F;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author nap
 *         20161015-10:19:48
 *         仓库产品控制器
 */
@Controller
@RequestMapping("/stock")
public class StockController extends BaseController{

    @Autowired
    StockService stockService;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private AreaService areaService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    DictionaryService dictionaryService;

    /**
     * 产品库存列表
     *
     * @return
     */
    @RequestMapping("/list")
    public List<StockDetail> list(Stock p) {
        return stockService.getProductStocks(p,getCurrentUser());
    }

    /**
     * 厂家未初始化入库的产品列表
     * @param p
     */
    @RequestMapping("/initStockProductList")
    public F.T3<List<Category>, List<SellLevel>, List<Product>> initStockProductList(Product p){
        return F.T3(categoryService.getAllCategorys(),
                dictionaryService.getAllSellLevels(),
                stockService.getUnInitStockProducts(p,getCurrentUser()));
    }

    /**
     * 提交添加产品库存(库存初始化操作，厂家原始库存)
     */
    @PostMapping(value = "/batchInit")
    public void postAdd(@ParamValid BatchStockInit b) throws BusinessException {
        User u=getCurrentUser();
        stockService.batchInitProductStock(b,u);
    }

    /**
     * 编辑产品库存(厂家修改原始库存)
     */
    @GetMapping(value = "/edit")
    public StockDetail edit(String stockId) {
        return stockService.getProductStock(stockId);
    }

    /**
     * 提交编辑产品库存(厂家修改原始库存)
     */
    @PostMapping(value = "/edit")
    public void postEdit(@ParamValid(StockEdit.class) Stock p) {
        stockService.editProductStock(p);
    }

    /**
     * 获取产品库存
     */
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Stock get(String stockId) {
        return stockService.getProductStock(stockId);
    }

    /**
     * 删除产品库存
     */
    @RequestMapping("/del")
    public void del(String stockId) {
        stockService.delProductStock(stockId);
    }

    /**
     * 上架
     * @param stockId
     */
    @PostMapping("/online")
    public void online(String stockId) throws BusinessException {
        stockService.onlineStock(getCurrentUser(), stockId);
    }

    /**
     * 下架
     * @param stockId
     */
    @PostMapping("/underline")
    public void underline(String stockId) throws BusinessException {
        stockService.underlineStock(getCurrentUser(), stockId);
    }

    /**
     * 调价
     * @param stock
     */
    @PostMapping("/changePrice")
    public void changePrice(Stock stock) throws BusinessException {
        stockService.changePrice(getCurrentUser(),stock.getStockId(),stock.getPrice());
    }
}