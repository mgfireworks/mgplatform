package com.mgfireworks.mgplatform.mgadmin.sys.model;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Area {
	/**
	 * 区域编号 省码/省码+市码/省码+市码+县码...
	 */
 	@NotNull(message="区域编号不能为空")
	private String areaId;
	
	/**
	 * 区域名称 
	 */
 	@NotNull(message="区域名称不能为空")
	private String areaName;
	
	/**
	 * 区域级别 0:省,1:市,2:县,3:区/镇,4:街道/村,5:组
	 */

	private Integer areaLevel;
	
	/**
	 * 父级编号 
	 */
 	@NotNull(message="父级编号不能为空")
	private String parentId;
	
 	/**
 	 * 父级名称
 	 * 新添加 zj
 	 */
 	private String parentArea;
}