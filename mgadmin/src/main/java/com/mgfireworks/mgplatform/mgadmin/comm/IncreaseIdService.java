package com.mgfireworks.mgplatform.mgadmin.comm;

import org.springframework.stereotype.Service;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import cn.hn.java.summer.utils.StringUtils;

/**
 * 顺序增长的id生成器
 * @author sjg
 * 2017年2月19日 下午9:24:21
 *
 */
@Service
public class IncreaseIdService extends BaseDao<DataSource1>{
	
	/**
	 * 取表某字段最大值+1作为新ID
	 * @param table
	 * @param col
	 * @return
	 */
	private Integer getNewId(String table, String col){
		String sql=StringUtils.format("select max({0}) from {1}",col,table);
		Integer maxId=this.get(sql, Integer.class);
		maxId=maxId==null?1:maxId+1;
		return maxId;
	}
	
	/**
	 * 取新的字典ID
	 * @return
	 */
	public Integer getNewDictionaryId(){
		return getNewId("mg_dictionary","dicId");
	}

	/**
	 * 取新的产品品牌ID
	 * @return
	 */
	public Integer getNewBandId(){
		return getNewId("mg_product_band","bandId");
	}
	
	/**
	 * 取新的产品分类ID
	 * @return
	 */
	public Integer getNewCategoryId(){
		return getNewId("mg_product_category","cateId");
	}
	
	/**
	 * 取新的机构ID
	 * @return
	 */
	public Integer getNewOrganizationId(){
		return getNewId("mg_organization","orgId");
	}
	
	/**
	 * 取新用户ID
	 * @return
	 */
	public Integer getNewUserId(){
		return getNewId("mg_user","userId");
	}
}
