package com.mgfireworks.mgplatform.mgadmin.sys.model;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Organization {
	/**
	 * 机构编号 
	 */
 	@NotNull(message="机构编号不能为空")
	private Integer orgId;
	
	/**
	 * 机构名称 
	 */
 	@NotNull(message="机构名称不能为空")
	private String orgName;

 	/**
 	 * 机构类型 0:平台,1:厂家,2:代理商
 	 */
 	@NotNull(message="机构类型不能为空")
 	private Integer orgType;
 	
 	/**
 	 * 父级机构
 	 */
 	private Integer parentId;
	
	/**
	 * 状态 0:未审核,1:已审核
	 */
 	@NotNull(message="状态不能为空")
	private Integer orgStatus;
	
	/**
	 * 机构地址 
	 */

	private String orgAddress;
	
	/**
	 * 负责人 
	 */

	private String orgOfficial;
	
	/**
	 * 联系方式 
	 */

	private String orgContact;
	
	/**
	 * 区域编号 
	 */
 	@NotNull(message="区域编号不能为空")
	private String areaId;

	/**
	 * 区域名
	 */
	private String areaName;

	/**
	 * 说明 
	 */

	private String description;

	public Organization(Integer orgId) {
		super();
		this.orgId = orgId;
	}

	public Organization() {
		super();
	}
	
	
}