package com.mgfireworks.mgplatform.mgadmin.product;

import cn.hn.java.summer.annotation.ParamValid;
import cn.hn.java.summer.exception.BusinessException;
import cn.hn.java.summer.mvc.ControllerAspectTool;
import com.mgfireworks.mgplatform.mgadmin.product.model.Band;
import com.mgfireworks.mgplatform.mgadmin.product.model.Category;
import com.mgfireworks.mgplatform.mgadmin.product.model.Product;
import com.mgfireworks.mgplatform.mgadmin.product.model.ProductDetail;
import com.mgfireworks.mgplatform.mgadmin.sys.DictionaryService;
import com.mgfireworks.mgplatform.mgadmin.sys.model.SellLevel;
import org.rythmengine.utils.F;
import org.rythmengine.utils.F.T3;
import org.rythmengine.utils.F.T5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 产品控制器
 */
@Controller
@RequestMapping("/product/product")
public class ProductController {

	@Autowired
	ProductService productService;
	@Autowired
	ProductDetailService productDetailService;
	@Autowired
	CategoryService categoryService;
	@Autowired
	BandService bandService;
	@Autowired
	DictionaryService dictionaryService;
	
	/**
	 * 产品列表
	 * @return 
	 * @return
	 */
	@RequestMapping("/list")
	public F.T4<List<Category>, List<Band>, List<SellLevel>, List<Product>> list(Product p){
		return F.T4(categoryService.getAllCategorys(),
				bandService.getAllBands(),
				dictionaryService.getAllSellLevels(),
				productService.getProducts(p));
	}

	/**
	 * 添加产品
	 */
	@GetMapping("/add")
	public T3<List<Category>, List<Band>, List<SellLevel>> add(){
		return F.T3(categoryService.getAllCategorys(), 
				bandService.getAllBands(), 
				dictionaryService.getAllSellLevels());
	}
	
	/**
	 * 提交添加产品
	 */
	@PostMapping("/add")
	public T3<List<Category>, List<Band>, List<SellLevel>> postAdd(
			@ParamValid Product p, @ParamValid ProductDetail pd) throws BusinessException {
		return ControllerAspectTool.swallowAndReturn(() -> productService.addProduct(p,pd),this::add);
	}

	/**
	 * 编辑产品
	 * @return 
	 * @return 
	 */
	@GetMapping("/edit")
	public T5<List<Category>, List<Band>, List<SellLevel>, Product, ProductDetail> edit(
			String productId){
		return F.T5(categoryService.getAllCategorys(), 
				bandService.getAllBands(), 
				dictionaryService.getAllSellLevels(),
				productService.getProduct(new Product(productId)),
				productDetailService.getProductDetail(new ProductDetail(productId)));
	}
	
	/**
	 * 提交编辑产品
	 * @return 
	 * @return 
	 */
	@PostMapping("/edit")
	public T3<List<Category>, List<Band>, List<SellLevel>> postEdit(
			@ParamValid Product p, @ParamValid ProductDetail pd) throws BusinessException {
		return ControllerAspectTool.swallowAndReturn(
		        () -> productService.editProduct(p,pd),
                () -> F.T3(categoryService.getAllCategorys(),
                        bandService.getAllBands(),
                        dictionaryService.getAllSellLevels()
                )
        );
	}
	
	/**
	 * 获取产品
	 */
	@GetMapping("/get")
	public Product get(Product p){
		return productService.getProduct(p);
	}
	
	/**
	 * 删除产品
	 */
	@PostMapping("/del")
	public void del(Product p){
		productService.delProduct(p);
	}
}