package com.mgfireworks.mgplatform.mgadmin.sys;

import cn.hn.java.summer.annotation.ParamValid;
import com.mgfireworks.mgplatform.BaseController;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 机构控制器
 */
@Controller
@RequestMapping("/sys/org")
public class OrganizationController extends BaseController{

	@Autowired
	OrganizationService organizationService;
	
	/**
	 * 机构列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<Organization> list(Organization p){
		return organizationService.getOrganizations(p);
	}
	
	/**
	 * 选择机构列表
	 * @return
	 */
	@RequestMapping("/select")
	public List<Organization> select(Organization p){
		return organizationService.getOrganizations(p);
	}

	/**
	 * 添加机构
	 */
	@GetMapping("/add")
	public Organization add(){
		return organizationService.newOrganization();
	}
	
	/**
	 * 提交添加机构
	 */
	@PostMapping("/add")
	public void postAdd(@ParamValid Organization p){
		organizationService.addOrganization(p);
	}

	/**
	 * 编辑机构
	 */
	@GetMapping("/edit")
	public Organization edit(Organization p){
		return organizationService.getOrganization(p);
	}
	
	/**
	 * 提交编辑机构
	 */
	@PostMapping("/edit")
	public void postEdit(@ParamValid Organization p){
		organizationService.editOrganization(p);
	}
	
	/**
	 * 获取机构
	 */
	@GetMapping("/get")
	public Organization get(Organization p){
		return organizationService.getOrganization(p);
	}
	
	/**
	 * 删除机构
	 */
	@PostMapping("/del")
	public void del(Organization p){
		organizationService.delOrganization(p);
	}


	///////////////////////////////代理商///////////////////////

	/**
	 * 下级代理商列表
	 * @return
	 */
	@RequestMapping("/subAgents")
	public List<Organization> subAgents(){
		return organizationService.getSubAgents(getCurrentUser());
	}
}