package com.mgfireworks.mgplatform.mgadmin.sys;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Area;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 区域业务类
 */
@Service
public class AreaService extends BaseDao<DataSource1>{
	
	/**
	 * 取区域列表
	 * @return
	 */
	public List<Area> getAreas(Area p){
		return list("getAreas",Area.class,p);
	}

	/**
	 * 添加区域
	 */
	public void addArea(Area p){
		update("addArea",p);
	}
	
	/**
	 * 修改区域
	 */
	public void editArea(Area p){
		update("editArea",p);
	}
	
	/**
	 * 删除区域
	 */
	public void delArea(Area p){
		update("delArea",p);
	}
	
	/**
	 * 取区域
	 */
	public Area getArea(Area p){
		return get("getArea",Area.class,p);
	}
	
}