package com.mgfireworks.mgplatform.mgadmin.stock;

import cn.hn.java.summer.annotation.ParamValid;
import cn.hn.java.summer.exception.BusinessException;
import cn.hn.java.summer.exception.CodeBusinessException;
import com.mgfireworks.mgplatform.BaseController;
import com.mgfireworks.mgplatform.mgadmin.product.BandService;
import com.mgfireworks.mgplatform.mgadmin.product.CategoryService;
import com.mgfireworks.mgplatform.mgadmin.product.ProductDetailService;
import com.mgfireworks.mgplatform.mgadmin.product.ProductService;
import com.mgfireworks.mgplatform.mgadmin.product.model.Category;
import com.mgfireworks.mgplatform.mgadmin.product.model.Product;
import com.mgfireworks.mgplatform.mgadmin.stock.model.Order;
import com.mgfireworks.mgplatform.mgadmin.stock.model.OrderItem;
import com.mgfireworks.mgplatform.mgadmin.stock.model.OrderItemModel;
import com.mgfireworks.mgplatform.mgadmin.stock.model.Stock;
import com.mgfireworks.mgplatform.mgadmin.sys.DictionaryService;
import com.mgfireworks.mgplatform.mgadmin.sys.OrganizationService;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Organization;
import org.rythmengine.utils.F;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by af on 2017/3/26.
 */
@Controller
@RequestMapping("/stock/order")
public class OrderController  extends BaseController{

    @Autowired
    private OrderService orderService;
    @Autowired
    ProductService productService;
    @Autowired
    ProductDetailService productDetailService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    BandService bandService;
    @Autowired
    DictionaryService dictionaryService;
    @Autowired
    OrganizationService organizationService;

    /**
     * 一级代理商进货列表
     */
    @RequestMapping("/agentOneIn")
    public  F.T3<List<Stock>, List<Organization>,List<Category>>
    getProductAgentOneForOrder(Product p) throws CodeBusinessException {
        return  F.T3(
                orderService.getStockProductForOrder(getCurrentUser(),p),
                organizationService.getAllFactories(),
                categoryService.getAllCategorys()
        );
    }

    /**
     * 二级代理商进货列表
     */
    @RequestMapping("/agentTwoIn")
    public  F.T3<List<Stock>, List<Organization>,List<Category>>
    getProductAgentTwoForOrder(Product p) throws CodeBusinessException {
        return getProductAgentOneForOrder(p);
    }

    /**
     * 添加订单项
     * @param item
     */
    @PostMapping("/addOrderItem")
    public void addOrderItem(@ParamValid OrderItem item) throws BusinessException {
        orderService.addOrderItem(getCurrentUser(),item);
    }

    /**
     * 订单列表
     * @return
     */
    @RequestMapping("/list")
    public List<Order> getOrders(Order order){
        return orderService.getUserOrderAndItemInfos(getCurrentUser(),order);
    }

    /**
     *  提交进货订单
     */
    @PostMapping("/checkout")
    public void  checkoutOrder(String orderId) throws BusinessException {
        orderService.checkoutOrder(orderId,getCurrentUser());
    }

    /**
     * 更新订单项
     */
    @PostMapping("/updateItems")
    public void updateOrderItems(OrderItemModel model) throws BusinessException {
        orderService.updateOrderItems(getCurrentUser(), model.getItems());
    }

    /**
     * 取消订单
     */
    @PostMapping("/cancel")
    public void cancel(String orderId) throws BusinessException {
        orderService.cancelOrder(orderId);
    }
}
