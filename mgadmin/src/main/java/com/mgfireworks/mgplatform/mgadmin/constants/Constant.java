package com.mgfireworks.mgplatform.mgadmin.constants;

import java.io.File;

/**
 * 常量类
 * @author sjg
 * 2016年10月10日 下午9:24:35
 *
 */
public interface Constant {

	/**
	 * 用户登录session键
	 */
	String USER_SESSION_KEY="USER_SESSION_KEY";
	
	/**
 	* 销售级别parentId
 	*/
 	int SELLlEVEL_PARENTID = 1;

	/**
	 * 上传时产品主图片标识
	 */
	int PRODUCT_MAIN_IMG=0;
 	/**
 	 * 产品主图片存放路径
	 * /static/images/p/m
 	 */
 	String PRODUCT_MAIN_IMG_DIR=File.separator+"static"+File.separator+"images"+File.separator+"p"+ File.separator+"m";

	/**
	 * 产品详情图片存放路径
	 * /static/images/p/d
	 */
	String PRODUCT_DETAIL_IMG_DIR=File.separator+"static"+File.separator+"images"+File.separator+"p"+ File.separator+"d";

	/**
	 * 产品类别图片存放路径
	 * /static/images/p/d
	 */
	String PRODUCT_CATE_IMG_DIR=File.separator+"static"+File.separator+"images"+File.separator+"p"+ File.separator+"c";


}
