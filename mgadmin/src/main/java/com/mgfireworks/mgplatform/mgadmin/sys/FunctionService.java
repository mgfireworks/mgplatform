package com.mgfireworks.mgplatform.mgadmin.sys;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Function;
import com.mgfireworks.mgplatform.mgadmin.user.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 功能业务类
 */
@Service
public class FunctionService extends BaseDao<DataSource1>{
	
	/**
	 * 取功能列表
	 * @return
	 */
	public List<Function> getFunctions(Function p){
		return list("getFunctions",Function.class,p);
	}

	/**
	 * 取用户所属权限的所有功能
	 * @return
	 */
//	@Cacheable(value="functions",key="'getUserRoleFunctions'+#user.userRole")
	public List<Function> getUserRoleFunctions(User user){
		return all("getUserRoleFunctions",Function.class,user.getUserRole());
	}

	/**
	 * 添加功能
	 */
	public void addFunction(Function p){
		update("addFunction",p);
	}
	
	/**
	 * 修改功能
	 */
	public void editFunction(Function p){
		update("editFunction",p);
	}
	
	/**
	 * 删除功能
	 */
	public void delFunction(Function p){
		update("delFunction",p);
	}
	
	/**
	 * 取功能
	 */
	public Function getFunction(Function p){
		return get("getFunction",Function.class,p);
	}
	
}