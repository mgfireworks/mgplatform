package com.mgfireworks.mgplatform.mgadmin.stock.model;

import lombok.Data;

/**
 * 产品库存详情
 */
@Data
public class StockDetail extends Stock{

    /**
     * 产品名
     */
    private String productName;

    /**
     * 类别名
     */
    private String cateName;

    /**
     * 品牌名
     */
    private String bandName;

    /**
     * 主图片
     */
    private String mainImg;

    /**
     * 销售级别名
     */
    private String sellLevelName;
}
