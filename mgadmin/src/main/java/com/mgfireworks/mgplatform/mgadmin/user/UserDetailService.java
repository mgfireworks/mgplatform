package com.mgfireworks.mgplatform.mgadmin.user;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;

import com.mgfireworks.mgplatform.mgadmin.user.model.UserDetail;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 用户详情业务类
 */
@Service
public class UserDetailService extends BaseDao<DataSource1>{
	
	/**
	 * 取用户详情列表
	 * @return
	 */
	public List<UserDetail> getUserDetails(UserDetail p){
		return list("getUserDetails",UserDetail.class,p);
	}

	/**
	 * 添加用户详情
	 */
	public void addUserDetail(UserDetail p){
		update("addUserDetail",p);
	}
	
	/**
	 * 修改用户详情
	 */
	public void editUserDetail(UserDetail p){
		update("editUserDetail",p);
	}
	
	/**
	 * 删除用户详情
	 */
	public void delUserDetail(UserDetail p){
		update("delUserDetail",p);
	}
	
	/**
	 * 取用户详情
	 */
	public UserDetail getUserDetail(UserDetail p){
		return get("getUserDetail",UserDetail.class,p);
	}
	
}