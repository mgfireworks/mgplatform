package com.mgfireworks.mgplatform.mgadmin.user;

import com.mgfireworks.mgplatform.BaseController;
import com.mgfireworks.mgplatform.mgadmin.constants.OrgType;
import com.mgfireworks.mgplatform.mgadmin.sys.OrganizationService;
import com.mgfireworks.mgplatform.mgadmin.sys.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 代理商管理
 */
@Controller
@RequestMapping("/user/agent")
public class AgentController extends BaseController{

	@Autowired
	AgentService agentService;

	@Autowired
	OrganizationService organizationService;

	/**
	 * 添加代理
	 */
	@RequestMapping("/addAgent")
	public void addAgent(){
	}

    /**
     * 提交添加代理
     */
    @PostMapping("/addAgent")
    public void postAddAgent(Organization org){
    }

	/**
	 * 获取二级代理商列表
	 * @return
	 */
	@RequestMapping("/selectAgents")
	public List<Organization> selectAgents(){
		Organization org=new Organization(OrgType.DELEGATE1.value());
		return organizationService.getOrganizations(org);
	}
}