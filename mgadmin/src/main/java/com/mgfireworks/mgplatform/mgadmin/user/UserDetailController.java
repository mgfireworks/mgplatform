package com.mgfireworks.mgplatform.mgadmin.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.hn.java.summer.annotation.ParamValid;

import com.mgfireworks.mgplatform.mgadmin.user.model.UserDetail;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 用户详情控制器
 */
@Controller
@RequestMapping("/user/userdetail")
public class UserDetailController {

	@Autowired
	UserDetailService userdetailService;
	
	/**
	 * 用户详情列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<UserDetail> list(UserDetail p){
		return userdetailService.getUserDetails(p);
	}

	/**
	 * 添加用户详情
	 */
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public void add(){
	}
	
	/**
	 * 提交添加用户详情
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public void postAdd(@ParamValid UserDetail p){
		userdetailService.addUserDetail(p);
	}

	/**
	 * 编辑用户详情
	 */
	@RequestMapping(value="/edit",method=RequestMethod.GET)
	public UserDetail edit(UserDetail p){
		return userdetailService.getUserDetail(p);
	}
	
	/**
	 * 提交编辑用户详情
	 */
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public void postEdit(@ParamValid UserDetail p){
		userdetailService.editUserDetail(p);
	}
	
	/**
	 * 获取用户详情
	 */
	@RequestMapping(value="/get",method=RequestMethod.GET)
	public UserDetail get(UserDetail p){
		return userdetailService.getUserDetail(p);
	}
	
	/**
	 * 删除用户详情
	 */
	@RequestMapping("/del")
	public void del(UserDetail p){
		userdetailService.delUserDetail(p);
	}
}