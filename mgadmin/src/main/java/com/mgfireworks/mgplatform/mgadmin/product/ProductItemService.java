package com.mgfireworks.mgplatform.mgadmin.product;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;

import com.mgfireworks.mgplatform.mgadmin.product.model.ProductItem;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 产品项目业务类
 */
@Service
public class ProductItemService extends BaseDao<DataSource1>{
	
	/**
	 * 取产品项目列表
	 * @return
	 */
	public List<ProductItem> getProductItems(ProductItem p){
		return list("getProductItems",ProductItem.class,p);
	}

	/**
	 * 添加产品项目
	 */
	public void addProductItem(ProductItem p){
		update("addProductItem",p);
	}
	
	/**
	 * 修改产品项目
	 */
	public void editProductItem(ProductItem p){
		update("editProductItem",p);
	}
	
	/**
	 * 删除产品项目
	 */
	public void delProductItem(ProductItem p){
		update("delProductItem",p);
	}
	
	/**
	 * 取产品项目
	 */
	public ProductItem getProductItem(ProductItem p){
		return get("getProductItem",ProductItem.class,p);
	}
	
}