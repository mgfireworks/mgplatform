package com.mgfireworks.mgplatform.mgadmin.constants;

public enum BooleanEnum {

	TRUE(1,"是"),
	FALSE(0,"否");

	private final int value;

	private final String phrase;

	private BooleanEnum(int value, String phrase) {
		this.value = value;
		this.phrase = phrase;
	}
	/**
	 * @return 状态的整型编码
	 */
	public int value() {
		return this.value;
	}

	/**
	 * @return 状态的描述
	 */
	public String getPhrase() {
		return this.phrase;
	}

	public static BooleanEnum valueOf(int v) {
		for (BooleanEnum val : values()) {
			if (val.value == v) {
				return val;
			}
		}
		return null;
	}

	/**
	 * 返回状态的编号
	 */
	@Override
	public String toString() {
		return phrase;
	}
}
