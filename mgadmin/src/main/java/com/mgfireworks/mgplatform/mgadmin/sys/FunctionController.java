package com.mgfireworks.mgplatform.mgadmin.sys;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.hn.java.summer.annotation.ParamValid;

import com.mgfireworks.mgplatform.mgadmin.sys.model.Function;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 功能控制器
 */
@Controller
@RequestMapping("/sys/function")
public class FunctionController {

	@Autowired
	FunctionService functionService;
	
	/**
	 * 功能列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<Function> list(Function p){
		return functionService.getFunctions(p);
	}

	/**
	 * 添加功能
	 */
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public void add(){
	}
	
	/**
	 * 提交添加功能
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public void postAdd(@ParamValid Function p){
		functionService.addFunction(p);
	}

	/**
	 * 编辑功能
	 */
	@RequestMapping(value="/edit",method=RequestMethod.GET)
	public Function edit(Function p){
		return functionService.getFunction(p);
	}
	
	/**
	 * 提交编辑功能
	 */
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public void postEdit(@ParamValid Function p){
		functionService.editFunction(p);
	}
	
	/**
	 * 获取功能
	 */
	@RequestMapping(value="/get",method=RequestMethod.GET)
	public Function get(Function p){
		return functionService.getFunction(p);
	}
	
	/**
	 * 删除功能
	 */
	@RequestMapping("/del")
	public void del(Function p){
		functionService.delFunction(p);
	}
}