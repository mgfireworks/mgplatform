package com.mgfireworks.mgplatform.mgadmin.product;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;

import com.mgfireworks.mgplatform.mgadmin.product.model.Band;

/**
 * 品牌
 * @author sjg
 * 2017年2月19日 下午8:11:33
 *
 */
@Service
public class BandService extends BaseDao<DataSource1>{

	/**
	 * 所有品牌
	 * @return
	 */
	public List<Band> getAllBands(){
		return this.all("getBands",Band.class);
	}
	
	/**
	 * 取品牌列表
	 * @return
	 */
	public List<Band> getBands(Band p){
		return list("getBands",Band.class,p);
	}

	/**
	 * 添加品牌
	 */
	public void addBand(Band p){
		update("addBand",p);
	}
	
	/**
	 * 修改品牌
	 */
	public void editBand(Band p){
		update("editBand",p);
	}
	
	/**
	 * 删除品牌
	 */
	public void delBand(Band p){
		update("delBand",p);
	}
	
	/**
	 * 取品牌
	 */
	public Band getBand(Band p){
		return get("getBand",Band.class,p);
	}
}
