package com.mgfireworks.mgplatform.mgadmin.product.model;

import cn.hn.java.summer.utils.StringUtils;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class ProductDetail {
	/**
	 * 产品编号 
	 */
 	@NotNull(message="产品编号不能为空")
	private String productId;
	
	/**
	 * 产品描述 
	 */

	private String description;
	
	/**
	 * 图片列表 以,隔开
	 */

	private String images;
	
	/**
	 * 详情 
	 */

	private String detailContent;

	public ProductDetail(String productId) {
		super();
		this.productId = productId;
	}

	public ProductDetail() {
		super();
	}

	/**
	 * 获取图片列表
	 * @return
	 */
	public List<String> getImageList(){
		if(StringUtils.isBlank(images)){
			return new ArrayList<>();
		}
		return Arrays.asList(images.split(";"));
	}
	
}