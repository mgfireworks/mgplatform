package com.mgfireworks.mgplatform.mgadmin.user.model;
import lombok.Data;

/**
 * 代理商关系
 */
@Data
public class AgentRelation {

	/**
	 * 代理商关系编号
	 */
	private String relationId;

	/**
	 * 机构编号
	 */
	private Integer orgId;

	/**
	 * 上级机构编号
	 */
	private Integer parentOrgId;
}