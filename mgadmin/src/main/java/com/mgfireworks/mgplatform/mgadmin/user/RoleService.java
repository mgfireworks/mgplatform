package com.mgfireworks.mgplatform.mgadmin.user;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import cn.hn.java.summer.utils.JsonUtils;
import com.mgfireworks.mgplatform.comm.StringUtils;
import com.mgfireworks.mgplatform.mgadmin.user.model.Role;
import com.mgfireworks.mgplatform.mgadmin.user.model.RoleFunc;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 用户角色业务类
 */
@Service
public class RoleService extends BaseDao<DataSource1>{
	
	/**
	 * 取用户角色列表
	 * @return
	 */
	public List<Role> getRoles(Role p){
		return list("getRoles",Role.class,p);
	}

	/**
	 * 获取所有角色
	 * @return
	 */
	public List<Role> getAllRoles(){
		return all("getRoles",Role.class);
	}

	/**
	 * 添加用户角色
	 */
	public void addRole(Role p){
		update("addRole",p);
	}
	
	/**
	 * 修改用户角色
	 */
	public void editRole(Role p){
		update("editRole",p);
	}
	
	/**
	 * 删除用户角色
	 */
	public void delRole(Role p){
		update("delRole",p);
	}
	
	/**
	 * 取用户角色
	 */
	public Role getRole(Role p){
		return get("getRole",Role.class,  p);
	}

	public Role getRole(Integer roleId){
		return get("getRole",Role.class,  roleId);
	}




	public List<RoleFunc> getRoleFunc(Integer roleId) {
		List<RoleFunc> rlist = list("getRoleFunc",RoleFunc.class);
		List<RoleFunc> chlidlist = list("getRoleFuncByRoleId",RoleFunc.class,roleId);
		rlist = StringUtils.checkList(rlist,chlidlist);
		Map<Integer,RoleFunc> map1 = new HashMap<>();
		Map<Integer,Set<RoleFunc>> map = new HashMap<>();
		for(RoleFunc r : rlist) {
			Set<RoleFunc> rs = new HashSet<>();
			if(r.getFunctionType()==1 && r.getParentId() == 0) {
				map1.put(r.getFunctionId(),r);
			}else if(r.getFunctionType()==2) {
				//之前存在的
				Set<RoleFunc> rs2 = map.get(r.getParentId());
				if(rs2 == null){
					//第一次注入
					rs.add(r);
					map.put(r.getParentId(),rs);
				} else {
					rs2.add(r);
				}
			} else {
				//异常
			}
		}
		List<RoleFunc> all =  new ArrayList<RoleFunc>();
		for (Integer key : map1.keySet()) {
			for(Integer key1 : map.keySet()) {
				if(key == key1) {
					RoleFunc r = map1.get(key);
					r.setNodes(map.get(key));
					all.add(r);
				}
			}
		}
		System.out.println(JsonUtils.toJson(all));
		return all;
	}

}