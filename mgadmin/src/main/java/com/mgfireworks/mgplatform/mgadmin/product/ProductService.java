package com.mgfireworks.mgplatform.mgadmin.product;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import cn.hn.java.summer.exception.BusinessException;
import cn.hn.java.summer.utils.DateUtils;
import cn.hn.java.summer.utils.IntUtils;
import com.mgfireworks.mgplatform.comm.StringUtils;
import com.mgfireworks.mgplatform.mgadmin.product.model.Product;
import com.mgfireworks.mgplatform.mgadmin.product.model.ProductDetail;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 产品业务类
 */
@Service
public class ProductService extends BaseDao<DataSource1>{
	
	/**
	 * 取产品列表
	 * @return
	 */
	public List<Product> getProducts(Product p){
		return list("getProducts",Product.class,p);
	}

	/**
	 * 添加产品
	 */
	@Transactional(rollbackFor=Exception.class)
	public void addProduct(Product p, ProductDetail pd) throws BusinessException {
		p.setAddTime(DateUtils.now(DateUtils.YMDHMS2));
		p.setUpdateTime(DateUtils.now(DateUtils.YMDHMS2));
		p.setProductId(newProductId(p));
		pd.setProductId(p.getProductId());

		//修复图片路径
		p.setMainImg(StringUtils.repairPath(p.getMainImg()));
		pd.setImages(StringUtils.repairPath(pd.getImages()));

		update("addProduct",p);
		update("addProductDetail",pd);
	}
	
	/**
	 * 修改产品
	 */
	@Transactional(rollbackFor=Exception.class)
	public void editProduct(Product p, ProductDetail pd){
		p.setUpdateTime(DateUtils.now(DateUtils.YMDHMS2));

        //修复图片路径
        p.setMainImg(StringUtils.repairPath(p.getMainImg()));
        pd.setImages(StringUtils.repairPath(pd.getImages()));

		update("editProduct",p);
		update("editProductDetail",pd);
	}

	/**
	 * 删除产品
	 */
	public void delProduct(Product p){
		update("delProduct",p);
	}
	
	/**
	 * 取产品
	 */
	public Product getProduct(Product p){
		return get("getProduct",Product.class,p);
	}
	
	/**
	 * 取新产品编号
	 * @param p
	 * @return
	 * @throws BusinessException 
	 */
	public String newProductId(Product p) throws BusinessException{
		if(p==null || p.getCateId()==null || p.getBandId()==null){
			throw new BusinessException("参数为空");
		}
		
		String maxPid= get("getMaxProductId", String.class, p);
		int index=1;
		if(maxPid!=null && maxPid.length()==3*4){
			index=IntUtils.parseInt(maxPid.substring(9), 1)+1;
		}
		//4位类别+4位品牌+4位序号
        String pid=StringUtils.leftPad(p.getCateId().toString(), 4, '0');
		pid+=StringUtils.leftPad(p.getBandId().toString(), 4, '0');
		pid+=StringUtils.leftPad(index+"", 4, '0');
		return pid;
	}
}