package com.mgfireworks.mgplatform.mgadmin.stock.model;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;

/**
 * Created by xw2sy on 2017-04-08.
 * 订单项详情
 */
@Data
public class OrderItem extends Order{
    /**
     * 订单项编号
     */
    private Integer itemId;

    /**
     * 对应产品库存ID
     */
    private String stockId;

    /**
     * 对应的产品procudtid
     */
    @NotBlank(message = "产品编号不能为空！")
    private String productId;

    /**
     * 产品名
     */
    private String productName;

    /**
     * 产品主图片
     */
    private String mainImg;

    /**
     * 单价,单位为元
     */
    private Double price;

    /**
     * 采购数量
     */
    @Min(value = 1,message = "采购数量不能为0！")
    private Integer num;
}
