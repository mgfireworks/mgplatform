package com.mgfireworks.mgplatform.mgadmin.constants;

/**
 * 区域级别
 * @author sjg
 * 2016年10月16日 下午2:52:50
 *
 */
public enum AreaLevel {
	PROVINCE(0,"省"),
	CITY(1,"市"),
	COUNTY(2,"县"),
	DISTRICT(4,"区/镇"),
	STREET(5,"街道/村"),
	GROUP(6,"小区/组");

	private final int value;

	private final String phrase;

	private AreaLevel(int value, String phrase) {
		this.value = value;
		this.phrase = phrase;
	}
	/**
	 * @return 状态的整型编码
	 */
	public int value() {
		return this.value;
	}

	/**
	 * @return 状态的描述
	 */
	public String getPhrase() {
		return this.phrase;
	}


	public static AreaLevel valueOf(int v) {
		for (AreaLevel val : values()) {
			if (val.value == v) {
				return val;
			}
		}
		return null;
	}

	/**
	 * 返回状态的编号
	 */
	@Override
	public String toString() {
		return phrase;
	}
	
}
