package com.mgfireworks.mgplatform.mgadmin.product;

import cn.hn.java.summer.annotation.ParamValid;
import com.mgfireworks.mgplatform.mgadmin.product.model.ProductDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 
 * @author nap
 * 20161015-10:19:48
 * 产品详情控制器
 */
@Controller
@RequestMapping("/product/productdetail")
public class ProductDetailController {

	@Autowired
	ProductDetailService productdetailService;
	
	/**
	 * 产品详情列表
	 * @return
	 */
	@RequestMapping("/list")
	public List<ProductDetail> list(ProductDetail p){
		return productdetailService.getProductDetails(p);
	}

	/**
	 * 添加产品详情
	 */
	@GetMapping("/add")
	public void add(){
	}
	
	/**
	 * 提交添加产品详情
	 */
	@PostMapping("/add")
	public void postAdd(@ParamValid ProductDetail p){
		productdetailService.addProductDetail(p);
	}

	/**
	 * 编辑产品详情
	 */
	@GetMapping("/edit")
	public ProductDetail edit(ProductDetail p){
		return productdetailService.getProductDetail(p);
	}
	
	/**
	 * 提交编辑产品详情
	 */
	@PostMapping("/edit")
	public void postEdit(@ParamValid ProductDetail p){
		productdetailService.editProductDetail(p);
	}
	
	/**
	 * 获取产品详情
	 */
	@GetMapping("/get")
	public ProductDetail get(ProductDetail p){
		return productdetailService.getProductDetail(p);
	}
	
	/**
	 * 删除产品详情
	 */
	@PostMapping("/del")
	public void del(ProductDetail p){
		productdetailService.delProductDetail(p);
	}
}