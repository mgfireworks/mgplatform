package com.mgfireworks.mgplatform.mgadmin.constants;

/**
 * 机构类型
 * @author sjg
 * 2017年2月26日 下午7:22:58
 *
 */
public enum OrgType {
	PLATFORM(0,"平台"),
	FACTORY(1,"厂家"),
	DELEGATE1(2,"一级代理"),
	DELEGATE2(3,"二级代理");

	private final int value;

	private final String phrase;

	OrgType(int value, String phrase) {
		this.value = value;
		this.phrase = phrase;
	}
	/**
	 * @return 整型编码
	 */
	public int value() {
		return this.value;
	}

	/**
	 * @return 描述
	 */
	public String getPhrase() {
		return this.phrase;
	}


	public static OrgType valueOf(int v) {
		for (OrgType val : values()) {
			if (val.value == v) {
				return val;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return phrase;
	}
	
}
