package com.mgfireworks.mgplatform.mgadmin.user.model;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UserDetail {
	/**
	 * 用户编号 
	 */
 	@NotNull(message="用户编号不能为空")
	private Integer userId;
	
	/**
	 * 邮箱 
	 */

	private String userMail;
	
	/**
	 * 用户图像 
	 */

	private String userAvatar;
	
}