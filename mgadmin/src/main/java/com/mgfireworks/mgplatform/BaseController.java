package com.mgfireworks.mgplatform;

import cn.hn.java.summer.mvc.WebContext;
import com.mgfireworks.mgplatform.mgadmin.constants.Constant;
import com.mgfireworks.mgplatform.mgadmin.user.model.User;

public abstract class BaseController extends cn.hn.java.summer.mvc.BaseController {

	static{
		//设置cookies存储的域路径(同一路径下cookies共享)
		WebContext.contextPath="/";
	}
	
	/**
	 * 取当前登录用户
	 * @return
	 */
	public User getCurrentUser(){
		User user= getSessionAttribute(Constant.USER_SESSION_KEY,User.class);
		if(user==null){
			redirectTo("/login");
		}
		return user;
	}
}
