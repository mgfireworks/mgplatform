package com.mgfireworks.mgplatform;

import cn.hn.java.summer.exception.BusinessException;
import com.mgfireworks.mgplatform.mgadmin.constants.Constant;
import com.mgfireworks.mgplatform.mgadmin.user.UserService;
import com.mgfireworks.mgplatform.mgadmin.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IndexController extends BaseController {

	@Autowired
	UserService userService;
	
	/**
	 * 登录页面
	 */
	@GetMapping("/login")
	public void login(){
	}
	
	/**
	 * 提交登录
	 * @throws BusinessException 
	 */
	@PostMapping("/login")
	public void postLogin(User u) throws BusinessException{
		addSessionAttribute(Constant.USER_SESSION_KEY, userService.login(u));
		redirectTo("/index");
	}

	/**
	 * 退出登录
	 */
	@RequestMapping("/logout")
	public void logout(){
		clearSession();
		redirectTo("/index");
	}
	
	/**
	 * 首页
	 */
	@RequestMapping("/index")
	public void index(){
	}
}
