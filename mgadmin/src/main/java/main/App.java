package main;

import cn.hn.java.summer.Config;
import cn.hn.java.summer.web.WebApp;
import com.mgfireworks.mgplatform.ActionInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import javax.servlet.MultipartConfigElement;

@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
@SpringBootApplication(scanBasePackages={"com.mgfireworks.mgplatform","cn.hn.java.summer.cache"})
@Configuration()
@EnableTransactionManagement
@EnableAspectJAutoProxy(proxyTargetClass=true)
@EnableScheduling
public class App extends WebApp
{
    public static void main( String[] args )
    {
    	//支持页面回退提交
		Config.isSupportBackPost=true;
    	SpringApplication.run(App.class);
    }

    /**
     * 配置拦截器
     * @author lance
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(createActionInterceptor()).addPathPatterns("/**");
    }

    @Bean
    public ActionInterceptor createActionInterceptor(){
        return new ActionInterceptor();
    }

    /**
     * 文件上传解析器
     * @return
     */
    @Bean
    public CommonsMultipartResolver multiPartResolver(){
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        return resolver;
    }

    /**
     * 文件上传配置
     * @param maxFileSize
     * @param maxRequestSize
     * @return
     */
    @Bean  
    public MultipartConfigElement multipartConfigElement(
    		@Value("${fileupload.maxFileSize}")
    		final String maxFileSize,
    		@Value("${fileupload.maxFileSize}")
    		final String maxRequestSize
    	) {  
        MultipartConfigFactory factory = new MultipartConfigFactory();  
        factory.setMaxFileSize(maxFileSize);  
        factory.setMaxRequestSize(maxRequestSize); 
        return factory.createMultipartConfig();  
    }
}