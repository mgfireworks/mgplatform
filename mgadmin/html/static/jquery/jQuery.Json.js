/**
 * Created by af on 2017/4/1.
 */
/**
 * @file            jQuery.Json.js
 * @description     用于支持Json与其它类型互转的扩展方法
 * @author          aof
 * @date            2017-04-01
 * @license         share
 * @version         1.0.20170401
 **/


/**
 * 将json字符串转换为对象的方法。
 *
 * @public
 * @param json字符串
 * @return 返回object,array,string等对象
 **/
jQuery.extend({
    /** * @see 将json字符串转换为对象 * @param json字符串 * @return 返回object,array,string等对象 */
    evalJSON: function(strJson) {
        return eval("(" + strJson + ")");
    }
});

/**
 * 将js数组转换成后台格式参数xx=[{a:"?",b:"?"},{a:"?",b:"?"}] --> xx[0].a=?&xx[0].b=?&xx[1]=?.a&xx[1].b=?
 *  arr--> 数组 arrName -->数组名称
 */
jQuery.extend({
    /** * @see 将json字符串转换为对象 * @param json字符串 * @return 返回object,array,string等对象 */
    evalToXml: function(arr,arrName) {
        if(!arr) {
            return "";
        }
        var str="";
        for(var i=0;i<arr.length;i++) {
            for (var Key in arr[i]){
                var name = arrName +'['+i+'].'
                str=str+''+name+Key+'='+arr[i][Key]+'&';
            }
        }
        str= str.substring(0,str.length-1)
        return str;
    }
});

/**
 * 将javascript数据类型转换为json字符串的方法。
 *
 * @public
 * @param  {object}  需转换为json字符串的对象, 一般为Json 【支持object,array,string,function,number,boolean,regexp *】
 * @return 返回json字符串
 **/
jQuery.extend({
    toJSONString: function(object) {
        var type = typeof object;
        // var type = Array == obj.constructor && 'Array' ||
        //     RegExp == obj.constructor && 'RegExp' ||
        //     typeof obj;
        if ('object' == type) {
            if (Array == object.constructor) type = 'array';
            else if (RegExp == object.constructor) type = 'regexp';
            else type = 'object';
        }
        switch (type) {
            case 'undefined':
            case 'unknown':
                return;
                break;
            case 'function':
            case 'boolean':
            case 'regexp':
                return object.toString();
                break;
            case 'number':
                return isFinite(object) ? object.toString() : 'null';
                break;
            case 'string':
                return '"' + object.replace(/(\\|\")/g, "\\$1").replace(/\n|\r|\t/g, function() {
                        var a = arguments[0];
                        return (a == '\n') ? '\\n': (a == '\r') ? '\\r': (a == '\t') ? '\\t': ""
                    }) + '"';
                break;
            case 'object':
                if (object === null) return 'null';
                var results = [];
                for (var property in object) {
                    var value = jQuery.toJSONString(object[property]);
                    if (value !== undefined) results.push(jQuery.toJSONString(property) + ':' + value);
                }
                return '{' + results.join(',') + '}';
                break;
            case 'array':
                var results = [];
                for (var i = 0; i < object.length; i++) {
                    var value = jQuery.toJSONString(object[i]);
                    if (value !== undefined) results.push(value);
                }
                return '[' + results.join(',') + ']';
                break;
        }
    }
});
