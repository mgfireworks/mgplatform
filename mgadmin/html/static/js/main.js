//存放全局数据
var globalData={
    //标识页面提交成功
    submitSuccess:false
}

//入栈当前url
navManage.push();

/**
 * 准备上传控件
 */
function preparedUploadController(id,maxFiles,type,onFinished) {
	var maxFiles=maxFiles||1;
	var onFinished=onFinished||function () {};
	var type=type||0;
    //图片上传
    $(id).dropzone({
        url: util.rootUrl()+"/sys/file/uploadImg.json?t="+type,
        maxFiles: maxFiles,
        maxFilesize: 1024*5,
        addRemoveLinks: true,
        dictDefaultMessage: "选择或拖动图片到此上传",
        dictFallbackMessage: "您的浏览器不支持拖动上传",
        dictInvalidFileType: "不允许上传此类型文件",
        dictFileTooBig: "文件超过大小限制",
        dictResponseError: "服务器响应错误：{{statusCode}}",
        dictCancelUpload: "取消",
        dictCancelUploadConfirmation: "确定取消上传？",
        dictMaxFilesExceeded: "只能上传"+maxFiles+"个文件",
        dictRemoveFile: "取消",
        acceptedFiles: ".jpg,.jpeg,.png,.gif",
        init: function() {
            this.on("success", function(file,result) {
                if(result && result.result){
                	var txt=$(this.element).prev();
                	var imgs=txt.val()+";"+result.data;
                    imgs=imgs.replace(/^;/,"");
                	txt.val(imgs);
                	//上传成功回调
                    onFinished.call(this,result.data);
                }
            });
        }
    });
}

//========================================================
$(function(){
	//菜单
	function slideDownMenu() {
        $(this).parent().addClass("active");
        var childMenu=$(this).closest(".child_menu").slideDown();
        childMenu.parent().addClass("active");
    }
    var isSideMenuSelected=false;
    $("#js-side-menu li>a").each(function() {
        var link = ($(this).attr("href") || "").replace(/\?.*/g, "");
        if(link && location.href.indexOf(link)>=0){
            isSideMenuSelected=true;
            slideDownMenu.call(this);
            return false;
		}
    }).click(function () {
        //清空导航
        navManage.clear();
    });
    //如果不能直接匹配url，则匹配模块
    if(!isSideMenuSelected){
        $("#js-side-menu li>a").each(function() {
            var link = ($(this).attr("href") || "").replace(/\?.*/g, "");
            if(link && location.href.substr(0,location.href.lastIndexOf("/")).indexOf(link.substr(0,link.lastIndexOf("/")))>=0){
                slideDownMenu.call(this);
                return false;
            }
        });
	}

	//处理表单页面采用ajax提交
    $("form.page-type-form").submit(function () {

        var data=$(this).serialize();
        var url=$(this).attr("action");
        if(url.indexOf("?")==0){
            //如果表单未设置提交地址，使用本页url
            url=window.location.href.replace(".html",".json");
            //剔除url中的重复参数，防止提交多个值
            var params=data.split(/\?|&/);
            for(var p in params){
                url=url.replace(params[p],"");
            }
        }
		// 使用 XMLHttpRequest 或者其他封装框架处理
		$.post(url,data,function (r) {
			console.log(r);
			if(!r){
				util.el.error("错误","提交失败，服务器错误！！");
				return;
			}
			if(r.result){
				util.el.successAndBack("提交成功！!!");
			}else{
				util.el.error("错误","提交失败！："+r.data);
			}
		});


        return false;
    });

    //使用ajax提交a标签所在部分的表单
    $(".js-json-post").click(function () {
        var that=this;
        var url=$(this).attr("href");
        var data=$("input,:checkbox:checked",$(this).closest(".js-partial-form"));
        //确认框
        var confirm=$(this).attr("confirm");
        if(confirm){
            confirm=$(confirm).html() || confirm;
            //替换变量
			$.each(data,function (i,n) {
				var name=$(n).attr("name");
				var value=$(n).val();
                confirm=confirm.replace(new RegExp("{"+name+"}","ig"),value);
            });
		}

		//提交方法
		function post() {
            data=data.serialize();
            var result=$(that).attr("result");
            util.request.post(url,data,function () {
                if(result){
                    if(result=="reload"){
                        util.el.successAndReload("提交成功！");
                    }else{
                        $(result).dialog();
                    }
                }else{
                    util.el.success("提交成功！");
                }
            });
        }

        if(confirm){
            util.el.confirm(confirm,post);
		}else{
        	post();
		}
        return false;
    });

    //页面查询按钮
	$(".jsPageQueryBtn").click(function () {
		var formData=$(this).attr("form_data")||".jsQueryForm input,.jsQueryForm select,.js-paging-page,.js-paging-pagesize";
		//只提交需要的数据
		$('<form action="?" method="post">').append($(formData)).submit();
		return false;
    });

    //弹出模态框的按钮
    $("[data-toggle='modal']").click(function () {
    	var target=$(this).attr("href");
    	if(target) {
            var data=$("input,:checkbox:checked",$(this).closest(".js-partial-form"));
            $target=$(target);
			//替换变量
			$.each(data,function (i,n) {
				var name=$(n).attr("name");
				var value=$(n).val();
				$("[name^='"+name+"']").val(value);
			});

            //如果目标元素不是已有的模态框，则创建
    		if(!$target.is(".modal")){
                $target.dialog();
			}else{
    			//使用目标元素自己的模态框
                $target.modal();
			}
        }
    	return false;
    });

	//设置下拉列表选中项
	$("select[value]").each(function(){
		$("option[value='"+$(this).attr("value")+"']",this).prop("selected",true);
	});
	
	//设置单选/多选按钮选中
	$(":radio,:checkbox").each(function(){
		var _this=$(this);
		_this.prop("checked",_this.attr("check")==_this.val());
	});
	
	//设置文本框只读
	$("input[read='true']").prop("readonly",true);
	
	//日期选择器
	$('.input-date-picker').daterangepicker({
		singleDatePicker: true,
		calender_style: "picker_3",
		format: "YYYY-MM-DD",
		locale : {  
            applyLabel : '确定',  
            cancelLabel : '取消',  
            fromLabel : '起始时间',  
            toLabel : '结束时间',  
            customRangeLabel : '自定义',  
            daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
            monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
                    '七月', '八月', '九月', '十月', '十一月', '十二月' ],  
            firstDay : 1  
        }  
   }).prop("readonly",true);
	
	//分页
	if(window["recordTotal"] || window["pageSize"]){
		//总页数
		var totalPage=parseInt(recordTotal/pageSize,10)+(recordTotal%pageSize?1:0);
		//显示开始记录
		var recordFrom=(currPage-1)*pageSize +1;
		recordFrom=recordFrom<=0?1:recordFrom;
		//显示结束记录
		var recordTo=parseInt(recordFrom)+parseInt(pageSize) -1;
		recordTo=recordTo>recordTotal?recordTotal:recordTo;
		
		if(currPage>totalPage){
			recordFrom=0;
			recordTo=0;
			currPage=1;
		}
		
		$("#span-paging-info-from").text(recordFrom);
		$("#span-paging-info-to").text(recordTo);
		$("#span-paging-info-total").text(recordTotal);
		
		$(".table-pagination-bar").pagination({
		  total_pages: totalPage,
		  current_page: currPage,
		  prev: "上一页",
		  next: "下一页",
		  callback: function(event, page) {
			  $("form .js-paging-page").val(page);
			  $("form .js-paging-pagesize").val(pageSize);
              $(".jsPageQueryBtn").click();
			  return false;
		  }
		});
		$("form")
		.append('<input type="hidden" class="js-paging-page" name="page" value="'+currPage+'"/>')
		.append('<input type="hidden" class="js-paging-pagesize" name="pageSize" value="'+pageSize+'"/>');
	}else{
		$(".table-pagination-info").hide();
	}

	//查询面板展开闭合切换
    $("#jsQuerySwitch").click(function () {
        $("#jsQueryPanel").removeClass("hidden-xs").toggleClass("hidden");
    });
	//窄屏默认不显示查询面板
	if($("#jsQueryPanel").css("display")=="none"){
        $("#jsQueryPanel").addClass("hidden");
	}

	/*工具栏*/
	$("#div_list_toolbar a").click(function(){
		var operate=$(this).text();
		var url=$(this).attr("href");
		//需要传参数
		var needParam=/\{\d\}/.test(url);
		var recordId=$(".td_row_id :checkbox:checked,.jsAutoCheckRow :checkbox:checked").val();
		if(recordId){
			recordId=recordId.split(",");
		}else{
			recordId=[];
		}
		
		if(needParam && recordId.length==0){
			util.el.error("请选择要"+operate+"的记录！");
			return false;
		}
		
		url=util.str.fmt(url, recordId);
		
		if(url.indexOf(".html")>0){
			//页面跳转
			location.href=url;
			return false;
		}else if(url.indexOf(".json")>0){
			//ajax
			util.el.confirm("确认要"+operate+"？",function(){
				$.post(url,function(r){
					if(r && r.result){
						util.el.successAndReload(operate+"成功！");
					}else{
						util.el.error(operate+"失败！");
					}
				});
			});
			return false;
		}
	});
	
	//取消返回按钮
	$(".jsBtnBack").unbind("click").click(function () {
        navManage.back(1);
        return false;
    });

    $("#js-form-error-list").hide();
	//===============表单操作处理
	//如果是表单提交，且不是从选择页面提交返回的，则显示提交成功提示
	if(!!window["_isFormPost"] && !window["_isPostBack"]){
        globalData.submitSuccess=false;
		//是提交返回的
		var errorList=$("#js-form-error-list");
		if($("li",errorList).length>0){
			errorList.show();
		}else{
			//标识提交成功
            globalData.submitSuccess=true;
			//提交成功弹出提示
			util.el.success("保存");
		}
	}

	//设置单选、复选框选中样式
	$('input[checked]').iCheck("check");
	$("input:checkbox").on("ifClicked",function () {
		if($(this).attr("name")=="__recordId"){
			//数据表格中行的选择操作
			$(this).closest(".table_row").toggleClass("selected");
			return false;
		}
    });

	//列表页面表格选择
	$("table .table_row").click(function () {
		$(":checkbox[name='__recordId']",$(this).parent()).iCheck("uncheck");
		$(this).toggleClass("selected");
		if($(this).is(".selected")){
            $(this).find(":checkbox").iCheck("check");
		}
    });

	//跳转到选择页面的按钮
	$(".btn-chooser").click(function(){
		var url=$(this).attr("href");
		//将本页表单数据和当前页面地址序列化提交传到选择页面
		var param=$("input:text,input:checkbox,input:radio,input:hidden[name!='__form'],select").serialize();
		$('<form method="post" action="'+url+'"></form>').append('<input type="hidden" name="__form" value="'+param+'"/>').submit();
		return false;
	});

	//选择结果返回按钮
    $(".jsBtnSelectBack").unbind("click").click(function() {
    	var postBackData=null;
    	//获取选中行单元格的内容(只取设置了name的单元格的内容)
        $(".table_row.selected td").each(function () {
			var td=$(this);
			var tdName=td.attr("name");
			if(!tdName){
				return;
			}
            postBackData=postBackData||{};
            postBackData[tdName]=td.text();
        });
        if(!!postBackData){
        	//有选择数据，提交返回
            util.el.postBack(postBackData);
        }else{
        	util.el.error("提示","还未选择内容！");
		}
        return false;
    });

    //输入框格式化输入
    $(":input[data-inputmask]").inputmask();

    //////////////////////////产品相关////////////////////////////
    //产品列表-选择产品
	$(".product-item").on("click",".caption",function (e) {
		//选中样式
	    var item=$(this).closest(".product-item").toggleClass("selected");
	    //选中ID
		$(":checkbox",item).prop("checked",item.is(".selected")	);
    }).on("click",".pi-buttons",function (e) {
        e.stopPropagation();
    })

    //图片缩放预览
    $(".img-scale-list").on("click","img",function () {
        $("#jsThumbnailImgPreview").remove();
        var imgPreview=$('<div id="jsThumbnailImgPreview" class="thumbnail-img-preview"><img src="'+$(this).attr("src")+'"/><div class="img-mask"></div></div>')
        imgPreview.appendTo(document.body);
        imgPreview.show(200);
        $("#jsThumbnailImgPreview").click(function () {
            $(this).remove();
        });
    });
});

$(function () {
    //页脚固定区域
    $("#jsFooterFixedSpace").css("height",$(".page-footer").height());

    //页面加载
    $("#jsContentPageLoading").fadeOut(1000);
    $("#appRoot").fadeIn(200);
});