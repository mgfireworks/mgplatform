/**
 * Created by xw2sy on 2017-03-18.
 */

//vue组件======================================
//表单-输入框
Vue.component('f_input', {
    props: {
        name:{
            type: String,
            default: ''
        },
        text:{
            type: String,
            default: ''
        },
        id:{
            type: String,
            default: ''
        },
        type:{
            type: String,
            default: 'text'
        },
        value:{
            type: String,
            default: ''
        },
        placeholder:{
            type: String,
            default: ''
        },
        required:{
            type: Boolean,
            default: false
        },
        readonly:{
            type: Boolean,
            default: false
        }
    },
    template: '#template-form-row-input'
});
//表单-文本域
Vue.component('f_textarea', {
    props: {
        name:{
            type: String,
            default: ''
        },
        text:{
            type: String,
            default: ''
        },
        id:{
            type: String,
            default: ''
        },
        type:{
            type: String,
            default: 'text'
        },
        value:{
            type: String,
            default: ''
        },
        placeholder:{
            type: String,
            default: ''
        },
        required:{
            type: Boolean,
            default: false
        },
        readonly:{
            type: Boolean,
            default: false
        }
    },
    template: $('#template-form-row-textarea').val()
});
//表单-选择器
Vue.component('f_chooser', {
    props: {
        name:{
            type: String,
            default: ''
        },
        text:{
            type: String,
            default: ''
        },
        id:{
            type: String,
            default: ''
        },
        value:{
            type: String,
            default: ''
        },
        name2:{
            type: String,
            default: ''
        },
        value2:{
            type: String,
            default: ''
        },
        url:{
            type: String,
            default: ''
        },
        placeholder:{
            type: String,
            default: ''
        },
        readonly:{
            type: Boolean,
            default: true
        }
    },
    template: '#template-form-row-chooser'
});
//表单-下拉框
Vue.component('f_select', {
    props: {
        name:{
            type: String,
            default: ''
        },
        text:{
            type: String,
            default: ''
        },
        id:{
            type: String,
            default: ''
        },
        value:{
            type: String,
            default: ''
        },
        required:{
            type: Boolean,
            default: false
        },
        readonly:{
            type: Boolean,
            default: false
        }
    },
    template: $('#template-form-row-select').val()
});
//表单-单选框
Vue.component('f_radio', {
    props: {
        name:{
            type: String,
            default: ''
        },
        text:{
            type: String,
            default: ''
        },
        text1:{
            type: String,
            default: ''
        },
        text2:{
            type: String,
            default: ''
        },
        value:{
            type: String,
            default: ''
        },
        value1:{
            type: String,
            default: ''
        },
        value2:{
            type: String,
            default: ''
        },
        required:{
            type: Boolean,
            default: false
        }
    },
    template: '#template-form-row-radio'
});

//查询页面表单组件================================
//搜索输入框
Vue.component('s_input', {
    props: {
        name:{
            type: String,
            default: ''
        },
        id:{
            type: String,
            default: ''
        },
        text:{
            type: String,
            default: ''
        },
        value:{
            type: String,
            default: ''
        },
        readonly:{
            type: Boolean,
            default: false
        }
    },
    template: '#template-search-form-col-input'
});
//搜索下拉框
Vue.component('s_select', {
    props: {
        name:{
            type: String,
            default: ''
        },
        id:{
            type: String,
            default: ''
        },
        text:{
            type: String,
            default: ''
        },
        value:{
            type: String,
            default: ''
        },
        deftext:{
            type: String,
            default: ''
        }
    },
    template: $('#template-search-form-row-select').val()
});
//搜索选择器
Vue.component('s_chooser', {
    props: {
        name:{
            type: String,
            default: ''
        },
        text:{
            type: String,
            default: ''
        },
        id:{
            type: String,
            default: ''
        },
        value:{
            type: String,
            default: ''
        },
        name2:{
            type: String,
            default: ''
        },
        value2:{
            type: String,
            default: ''
        },
        url:{
            type: String,
            default: ''
        },
        placeholder:{
            type: String,
            default: ''
        },
        readonly:{
            type: Boolean,
            default: true
        }
    },
    template: '#template-search-form-col-chooser'
});
//搜索面板
Vue.component('s_panel', {
    template: "#template-search-panel"
});
Vue.component('s_buttons', {
    props:{
        cols:{
            type: Number,
            default: 0
        }
    },
    template: "#template-search-buttons"
});
Vue.component('s_button', {
    props:{
        type:{
            type: String,
            default: ''
        },
        id:{
            type: String,
            default: ''
        },
        icon:{
            type: String,
            default: 'fa-search'
        },
        href:{
            type: String,
            default: ''
        },
        col:{
            type: Number,
            default: 0
        },
        //表单数据，使用jquery选择器指定将界面上哪些元素作为表单数据来源
        form_data:{
            type: String,
            default: ''
        }
    },
    template: "#template-search-button"
});


//搜索列表页面标题
Vue.component('s_title', {
    props: {
        title:{
            type: String,
            default: '标题'
        }
    },
    template: "#template-search-title"
});

//数据表格
Vue.component('d_table', {
    props: {
        minWidth:{
            type: Number,
            default: 0
        },
        cols:{
            type: Number,
            default: 20
        }
    },
    template: $('#template-list-data-grid').val()
});
Vue.component('d_head', {
    template: '<thead><slot></slot></thead>'
});
Vue.component('d_body', {
    template: '<tbody><slot></slot></tbody>'
});
Vue.component('d_tr', {
    props: {
        cls:{
            type: String,
            default: ''
        }
    },
    template: '<tr :class="cls" role="row"><slot></slot></tr>'
});
Vue.component('d_th', {
    props: {
        cls:{
            type: String,
            default: ''
        }
    },
    template: '<th :class="cls"><slot></slot></th>'
});
Vue.component('d_td', {
    props: {
        cls:{
            type: String,
            default: ''
        },
        name:{
            type: String,
            default: ''
        }
    },
    template: '<td :class="cls" :name="name"><slot></slot></td>'
});

//数字增减输入框
Vue.component('num_changer', {
    props: {
        name:{
            type: String,
            default: ''
        },
        id:{
            type: String,
            default: ''
        },
        value:{
            type: Number,
            default: 0
        }
    },
    template: '#template-input-number-changer'
});
