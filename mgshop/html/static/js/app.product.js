/**
 * Created by xw2sy on 2017-05-01.
 */
var lastPic=$("#jsPicView > div:last").clone().addClass("mui-slider-item-duplicate");
var fistPic=$("#jsPicView > div:first").clone().addClass("mui-slider-item-duplicate");
$("#jsPicView").prepend(lastPic).append(fistPic);
mui.previewImage();

//添加到购物车
util.el.tap("#jsAddToCart",function() {
    var data=JSON.parse($(this).attr("data"));
    $.post("/order/addToCart.json",data,function(r) {
        if(r.result){
            mui.toast('已添加到购物车！');
        }else{
            mui.alert(r.data,"错误","确定",function () {
                location.href="/address_setting.html";
            });
        }
    });
});