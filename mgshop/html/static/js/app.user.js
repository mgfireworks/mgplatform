/**
 * Created by xw2sy on 2017-05-01.
 */
app.user={};

(function () {

    var userInfoKey="userInfo";
    /**
     * 取用户信息
     * @param callback
     */
    app.user.getUserInfo=function (callback) {
        var userInfo = sessionStorage.getItem(userInfoKey);
        if (!userInfo) {
            //去服务器取
            $.get("/getUserInfo.json", function (r) {
                if(r.result){
                    userInfo=JSON.stringify(r.data);
                    sessionStorage.setItem(userInfoKey,userInfo);
                    callback(r.data);
                }
            });
        }else{
            userInfo = JSON.parse(userInfo);
            callback(userInfo);
        }
    };

    /**
     * 保存用户信息
     * @param userInfo
     */
    app.user.setUserInfo=function (userInfo) {
        sessionStorage.setItem(userInfoKey,JSON.stringify(userInfo));
    }

})();