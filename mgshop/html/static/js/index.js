//工具类
var util={
	el:{
		tap:function(el,fun){
			var target=mui(el)[0];
			if(!target){
				return;
			}
			target.addEventListener("tap",fun);
		},
        /**
         * 使用模板加载数据
         * @param id
         * @param data
         * @param callback
         */
		templateLoad:function (id,data,callback) {
            util.el.loading();
            var tp=$("#"+id);
            var url=tp.attr("url");
            var target=tp.attr("target");
            var resultTarget=target?$(target):tp.next();
            //直接使用数据呈现模板内容
            if(!url){
                resultTarget.html(template(id, data));
                util.el.loaded();
                return;
            }
            //请求url返回数据呈现模板内容
            $.post(url,data,function (r) {
                if(!r.result){
                    mui.alert("加载数据失败！","错误","确定");
                    return;
                }
                var html = $.trim(template(id, r));
                if(html){
                    resultTarget.html(html);
                }
                if(callback){
                    callback(r.data);
                }
				util.el.loaded();
            });
        },
		loading:function () {
            $("#loading-animate").find(".object").addClass("loading").end().show();
        },
		loaded:function () {
            $("#loading-animate").find(".object").removeClass("loading").end().hide();
        }
	},
    request:{
	    post:function (url,data,onSuccess,onFail) {
	        util.el.loading();
	        url=appUriRoot+url;
            onSuccess=onSuccess||function () {
                mui.toast("操作成功！");
            };
            onFail=onFail||function (msg) {
                mui.toast("操作失败！"+msg);
            };
            $.post(url,data,function (r) {
                if(r && r.result){
                    onSuccess(r.data);
                }else{
                    onFail(r.data);
                }
                util.el.loaded();
            });
        }
    }
}
app.isTest=function () {
    return location.href.indexOf("localhost")>=0;
}

//导入artTemplate 日期处理类变量
template.defaults.imports.date_format=fecha.format;
template.defaults.imports.date_parse=fecha.parse;

//初始化ui
mui.init({swipeBack:true});

var gallery = mui('.mui-slider');
gallery.slider({
  interval:5000//自动轮播周期，若为0则不自动播放，默认为0；
});

//主界面和侧滑菜单界面均支持区域滚动；
mui('.mui-scroll-wrapper').scroll();

mui('body').on("tap","a",function () {
	var href=this.getAttribute("href");
	if(href){
		location.href=href;
	}
});

//选中当前菜单
$("#js-nav-bottom .js-menu-"+globalData.currentMenu).addClass("mui-active");

//顶部地址设置按钮
util.el.tap("#js-btn-address-set",function(){
    location.href=appUriRoot+ "address_setting.html";
});

//打开产品搜索窗口
util.el.tap("#jsInputSearch",function () {
    mui.openWindow({
        url:"/search.html",
        id:"page-search",
        extras:{
        },
        show: {
            aniShow: 'slide-in-right'
        },
        waiting:{
            autoShow:true,//自动显示等待框，默认为true
            title:'正在加载...'//等待对话框上显示的提示内容
        }
    });
});


(function () {

    //用户信息加载回调列表
    var userInfoLoadCallbacks=[];

	app.index={
        /**
		 * 用户信息加载回调
         * @param callback
         */
		onUserInfoLoad:function (callback) {
            userInfoLoadCallbacks.push(callback);
        },
        //用户信息加载回调
        userInfoLoadCallback:function (userInfo) {
            // alert(addr.province + ", " + addr.city + ", " + addr.district + ", " + addr.street + ", " + addr.streetNumber);
            $("#spanClientAddress").text(userInfo.street||userInfo.district||userInfo.city);
            $.each(userInfoLoadCallbacks,function (i,n) {
                n(userInfo);
            });
            if(userInfoLoadCallbacks.length==0){
                util.el.loaded();
            }
        }
	}
})();

$(function () {
    sessionStorage.clear();
    //取用户信息
    app.user.getUserInfo(function (userInfo) {
        if(!userInfo){
            mui.alert("获取用户信息失败","错误","确定");
            return;
        }

        //有定位信息
        if(userInfo.latAndLng!=null){
            //用户信息加载回调
            app.index.userInfoLoadCallback(userInfo);
            return;
        }

        //获取用户当前位置信息
        app.address.getAddress(function(addr){
            //保存用户位置
            $.post("/updateUserGeography.json",addr.point,function (r) {
                if(!r.result){
                    return;
                }
                app.user.setUserInfo(r.data);
                //用户信息加载回调
                app.index.userInfoLoadCallback(r.data);
            });
        });

    });
});