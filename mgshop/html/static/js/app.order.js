/**
 * Created by xw2sy on 2017-05-01.
 */
app.index.onUserInfoLoad(function(){
    //订单列表
    util.el.templateLoad("jsTemplateOrders",null,function (data) {
        //计算总数
        var sum={total:0,amount:0};
        $.each(data,function (i,n) {
            sum.total+=n.amount;
            sum.amount+=n.amount*n.price;
        });
        util.el.templateLoad("jsTemplateCartSummery",sum);
        mui(".mui-numbox").numbox();
        if(sum.total>0){
            $("#jsSettlement").removeClass("mui-hidden");
        }
    });

    //去结算
    util.el.tap("#jsSettlement",function () {
        util.request.post("order/newOrder.json",{},function (data) {
            mui.openWindow({
                url:appUriRoot+"order/settlement.html?orderId="+data.orderId
            });
        });
    });
});