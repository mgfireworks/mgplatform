app.address={};

~function () {
    var getLocation = function (successFunc, errorFunc) { //successFunc获取定位成功回调函数，errorFunc获取定位失败回调
        if(app.isTest()){
            successFunc({province:"广东省",city:"深圳市",district:"南山区",street:"茶光路",point:{"lng":113.958437,"lat":22.57193}});
            return;
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                    var lng = position.coords.longitude;
                    var lat = position.coords.latitude;
                    var ggPoint = new BMap.Point(lng,lat);
                    var convertor = new BMap.Convertor();
                    //需要将原始的gps
                    convertor.translate([ggPoint], 1, 5, function (data) {
                        if(data.status === 0) {
                            var point = data.points[0];
                            var gc = new BMap.Geocoder();
                            gc.getLocation(point, function (rs) {
                                var addComp = rs.addressComponents;
                                addComp.point=point;
                                if (successFunc != undefined){
                                    successFunc(addComp);
                                }
                            });
                        }
                    });

                },
                function (error) {
                    switch (error.code) {
                        case 1:
                            alert("位置服务被拒绝。");
                            break;
                        case 2:
                            alert("暂时获取不到位置信息。");
                            break;
                        case 3:
                            alert("获取位置信息超时。");
                            break;
                        default:
                            alert("未知错误。");
                            break;
                    }
                    if (errorFunc != undefined)
                        errorFunc(error);
                }, { timeout: 5000, enableHighAccuracy: true });
        } else {
            alert("你的浏览器不支持获取地理位置信息。");
            if (errorFunc != undefined)
                errorFunc("你的浏览器不支持获取地理位置信息。");
        }
    };
    var showPositionError = function (error) {
        switch (error.code) {
            case 1:
                alert("位置服务被拒绝。");
                break;
            case 2:
                alert("暂时获取不到位置信息。");
                break;
            case 3:
                alert("获取位置信息超时。");
                break;
            default:
                alert("未知错误。");
                break;
        }
    };

    var client_address="client_address";
    /**
     * 取终端定位地址
     */
    app.address.getAddress=function(callback){
        var clientAddress=sessionStorage.getItem(client_address)||"{}";
        clientAddress=JSON.parse(clientAddress);
        //取到存储的地址,如果没有则实时取
        if(!clientAddress.province || !clientAddress.city){
            getLocation(function(addr){
                sessionStorage.setItem(client_address, JSON.stringify(addr));
                callback(addr);
            },function(error){
                showPositionError(error);
                //sessionStorage.setItem(client_address, JSON.stringify(remote_ip_info));
                //callback(remote_ip_info);
            });
        }else{
            callback(clientAddress);
        }
    };

    /**
     * 存储地址信息
     * @param address
     */
    app.address.setAddress=function (address) {
        sessionStorage.setItem(client_address,address);
    }
}();
