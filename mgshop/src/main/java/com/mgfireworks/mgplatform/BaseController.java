package com.mgfireworks.mgplatform;

import cn.hn.java.summer.mvc.WebContext;
import com.mgfireworks.mgplatform.mgshop.comm.Constants;
import com.mgfireworks.mgplatform.mgshop.main.AccountService;
import com.mgfireworks.mgplatform.mgshop.model.ShopUserInfoVo;
import com.mgfireworks.mgplatform.mgshop.model.ShopUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BaseController extends cn.hn.java.summer.mvc.BaseController {

	static{
		//设置cookies存储的域路径(同一路径下cookies共享)
		WebContext.contextPath="/";
	}

	@Autowired
	private AccountService accountService;
	
	/**
	 * 取当前登录用户信息
	 * @return
	 */
	public ShopUserInfoVo getCurrentUser(){
		String userAccount=getSessionAttribute(Constants.USER_SESSION_KEY,String.class);;
		ShopUserVo user=accountService.getShopUser(userAccount);
		ShopUserInfoVo userInfoVo=accountService.getShopUserInfo(user.getUserId());
		if(user==null){
			redirectTo("/login");
		}
		return userInfoVo;
	}
}
