package com.mgfireworks.mgplatform.mgshop.main;

import cn.hn.java.summer.exception.BusinessException;
import com.mgfireworks.mgplatform.BaseController;
import com.mgfireworks.mgplatform.mgshop.model.OrderItemDetail;
import com.mgfireworks.mgplatform.mgshop.model.ShopOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by xw2sy on 2017-04-30.
 */
@Controller()
@RequestMapping("/order")
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;

    /**
     * 添加到购物车
     * @param stockId
     */
    @PostMapping("/addToCart")
    public void addToCart(String stockId) throws BusinessException {
        orderService.addProductToCart(getCurrentUser(),stockId);
    }

    /**
     * 我的购物车页面
     */
    @RequestMapping("/myCart")
    public void myCart(){}

    /**
     * 购物车项列表
     * @return
     */
    @RequestMapping("/cartItems")
    public List<OrderItemDetail> getCartItems(){
        return orderService.getCartItems(getCurrentUser());
    }

    /**
     * 使用购物车新建订单
     */
    @PostMapping("/newOrder")
    public ShopOrderVo newOrder() throws BusinessException {
        return orderService.submitOrder(getCurrentUser());
    }

    /**
     * 我的订单页面
     */
    @RequestMapping("/myOrder")
    public void myOrder(){
    }

    /**
     * 取用户订单列表
     * @return
     */
    @PostMapping("/getOrders")
    public List<ShopOrderVo> getOrders(){
        return orderService.getOrders(getCurrentUser());
    }

    /**
     * 订单详情
     * @param orderId 订单号
     */
    @RequestMapping("/orderInfo")
    public void orderInfo(String orderId){
    }

    /**
     * 取订单详情
     * @param orderId 订单号
     * @return
     */
    @RequestMapping("/getOrderInfo")
    public ShopOrderVo getOrderInfo(String orderId){
        return orderService.getOrderInfo(getCurrentUser(),orderId);
    }
}
