package com.mgfireworks.mgplatform.mgshop.main;

import cn.hn.java.summer.exception.BusinessException;
import com.mgfireworks.mgplatform.BaseController;
import com.mgfireworks.mgplatform.mgshop.comm.Constants;
import com.mgfireworks.mgplatform.mgshop.model.ShopUserVo;
import com.mgfireworks.mgplatform.mgshop.model.weixin.WeiXinLogin;
import com.mgfireworks.mgplatform.mgshop.model.weixin.WeiXinValidInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by xw2sy on 2017-04-29.
 */
@Controller()
@RequestMapping("/account")
public class AccountController extends BaseController{

    @Autowired
    private AccountService accountService;

    /**
     * 微信接入验证接口
     * @param info 微信接入验证信息
     * @return echostr
     */
    @RequestMapping("/weiXinAccessValid")
    @ResponseBody
    public String weiXinAccessValid(WeiXinValidInfo info){
        return accountService.weiXinAccessValid(info);
    }

    /**
     * 微信用户登录回调
     * @param info 登录信息
     */
    @RequestMapping("/weiXinOAuthCallBack")
    public void weiXinOAuthCallBack(WeiXinLogin info) throws BusinessException {
        logger.info("=================="+info.getCode());
        //登录并获取微信用户信息
        ShopUserVo user= accountService.loginToWeiXin(info);
        //清除缓存
        accountService.removeShopUserCache(user.getUserAccount());
        //保存session
        addSessionAttribute(Constants.USER_SESSION_KEY,user.getUserAccount());
        redirectTo("/index");
    }

}
