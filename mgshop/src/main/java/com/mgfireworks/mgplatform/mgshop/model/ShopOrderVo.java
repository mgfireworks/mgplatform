package com.mgfireworks.mgplatform.mgshop.model;

import com.mgfireworks.mgplatform.mgshop.comm.OrderStatus;
import com.mgfireworks.mgplatform.mgshop.db.ShopOrder;
import lombok.Data;

import java.util.List;

/**
 * Created by xw2sy on 2017-05-05.
 */
@Data
public class ShopOrderVo extends ShopOrder {

    private String productName;

    private String mainImg;

    private List<OrderItemDetail> items;

    public String getStatusName(){
        if(getStatus()==null){
            return null;
        }
        OrderStatus os=OrderStatus.valueOf(getStatus());
        if(os==null){
            return null;
        }
        return os.getPhrase();
    }
}
