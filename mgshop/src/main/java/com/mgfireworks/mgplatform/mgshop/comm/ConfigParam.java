package com.mgfireworks.mgplatform.mgshop.comm;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by xw2sy on 2017-04-03.
 */
@Component
@Data
public class ConfigParam {

    /**
     * 套餐类别所属编号
     */
    @Value("${shop.taocan.id}")
    private Integer taoCanCateId;
}
