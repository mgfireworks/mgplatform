package com.mgfireworks.mgplatform.mgshop.main;

import cn.hn.java.summer.db.BaseDao;
import cn.hn.java.summer.db.multiple.DataSource1;
import com.mgfireworks.mgplatform.mgshop.comm.ConfigParam;
import com.mgfireworks.mgplatform.mgshop.model.Product;
import com.mgfireworks.mgplatform.mgshop.model.ProductDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService extends BaseDao<DataSource1>{
	@Autowired
	private ConfigParam configParam;

	/**
	 * 取二级代理商的首页推荐商品列表
	 * @param orgId 代理商id
	 * @return
	 */
	public List<Product> getRecommendProducts(int orgId){
		//要根据地理位置获取二级代理商的orgId
		return this.all("getIndexRecommendProducts", Product.class,ce().set("orgId",orgId));
	}

	/**
	 * 取产品详情
	 * @param stockId
	 * @return
	 */
	public ProductDetail getProductDetail(String stockId){
		return this.get("getProductDetail",ProductDetail.class,ce().set("stockId",stockId));
	}

	/**
	 * 取二级代理商套餐类产品列表
	 * @param orgId
	 * @return
	 */
	public List<Product> getTaoCanProductList(int orgId){
		return this.list(
			"getProductListByOrgIdCateId",
			Product.class,
				ce().set("orgId",orgId)
					.set("cateId",configParam.getTaoCanCateId())
		);
	}

	/**
	 * 取二级代理商产品分类中的产品列表
	 * @param orgId
	 * @return
	 */
	public List<Product> getCateProductList(int orgId, int cateId){
		return this.list(
			"getProductListByOrgIdCateId",
			Product.class,
				ce().set("orgId",orgId)
				.set("cateId",cateId)
		);
	}

}
