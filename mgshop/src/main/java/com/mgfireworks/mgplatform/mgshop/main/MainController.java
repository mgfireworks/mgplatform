package com.mgfireworks.mgplatform.mgshop.main;

import cn.hn.java.summer.exception.BusinessException;
import cn.hn.java.summer.mvc.WebContext;
import com.mgfireworks.mgplatform.BaseController;
import com.mgfireworks.mgplatform.mgshop.comm.Constants;
import com.mgfireworks.mgplatform.mgshop.model.*;
import com.mgfireworks.mgplatform.mgshop.model.map.MapPoint;
import com.mgfireworks.mgplatform.mgshop.model.weixin.WeiXinAppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class MainController extends BaseController{
	@Autowired
	private ProductService productService;
	@Autowired
	private AreaService areaService;
	@Autowired
	private WeiXinAppConfig weiXinAppConfig;
	@Autowired
	private AccountService accountService;
	
	/**
	 * 首页
	 */
	@RequestMapping("/index")
	public void index(){
	}

	/**
	 * 取店铺用户信息
	 * @return
	 */
	@RequestMapping("/getUserInfo")
	public ShopUserInfoVo getShopUserInfo(){
		return getCurrentUser();
	}

	/**
	 * 更新用户地理位置信息
	 * @param point
	 */
	@PostMapping("/updateUserGeography")
	public ShopUserInfoVo updateUserGeography(MapPoint point) throws BusinessException {
		ShopUserInfoVo user= getCurrentUser();
		user.setPoint(point);
		//更新缓存
		accountService.updateShopUserInfoCache(user.getUserId(),user);
		if(user.getShopOrgId()==null){
			throw new BusinessException("该区域暂未开通店铺，请重设地址！");
		}
		return user;
	}

	@RequestMapping("/mall")
	public List<Product> mall(){
		ShopUserInfoVo user=getCurrentUser();
		//烟花套餐产品列表
		return productService.getTaoCanProductList(user.getShopOrgId());
	}

	@RequestMapping("/search")
	public void search(){
	}

	@RequestMapping("/service")
	public void service(){}

	@RequestMapping("/login")
	public WeiXinAppConfig login(){
		return weiXinAppConfig;
	}

	@RequestMapping("/test")
	public void test(){
		ShopUserVo user=accountService.getShopUser("ok1c30TZ6yTxmPJ_S-kE8Ojy2cMc");
		WebContext.addSessionCookie(Constants.USER_SESSION_KEY,user.getUserAccount());
		redirectTo("/index.html");
	}

	/**
	 * 烟花超市分类产品列表
	 * @param cateId
	 * @return
	 */
	@RequestMapping(value = "/market/{cateId}",name = "/market")
	public List<Product> market(@PathVariable Integer cateId){
		ShopUserInfoVo user= getCurrentUser();
		return productService.getCateProductList(user.getShopOrgId(),cateId);
	}

	/**
	 * 地址设置
	 */
	@RequestMapping("/address_setting")
	public void addressSetting(){
	}

	@RequestMapping(value = "/p/{stockId}",name = "/p")
	public ProductDetail productDetail(@PathVariable String stockId){
		return productService.getProductDetail(stockId);
	}
}
