package com.mgfireworks.mgplatform.mgshop.model;
import lombok.Data;

@Data
public class Product {

	/**
	 * 产品库存编号
	 */
	private String stockId;

	/**
	 * 产品编号 4位cateId+4位bandId+4位序号
	 */
	private String productId;
	
	/**
	 * 产品名 
	 */
	private String productName;
	
	/**
	 * 类别 
	 */
	private Integer cateId;
	
	/**
	 * 类别名 
	 */
	private String cateName;
	
	/**
	 * 品牌 
	 */
	private Integer bandId;
	
	/**
	 * 品牌名 
	 */
	private String bandName;
	
	/**
	 * 主图片 
	 */
	private String mainImg;
	
	/**
	 * 销售级别 对应字典项中销售级别值
	 */
	private Integer sellLevel;

	/**
	 * 销售级别名
	 */
	private  String sellLevelName;
	
	/**
	 * 是否推荐 0:不推荐,1:推荐
	 */
	private Integer recommend;

	/**
	 * 价格
	 */
	private Double price;

	/**
	 * 产品描述 
	 */
	private String description;
}