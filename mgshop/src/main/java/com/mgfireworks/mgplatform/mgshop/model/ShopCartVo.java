package com.mgfireworks.mgplatform.mgshop.model;

import com.mgfireworks.mgplatform.mgshop.db.ShopCart;
import lombok.Data;

/**
 * Created by xw2sy on 2017-04-30.
 */
@Data
public class ShopCartVo extends ShopCart {
}
