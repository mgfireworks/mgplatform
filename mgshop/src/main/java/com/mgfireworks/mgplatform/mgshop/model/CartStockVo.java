package com.mgfireworks.mgplatform.mgshop.model;

import com.mgfireworks.mgplatform.mgshop.db.ProductStock;
import lombok.Data;

/**
 * Created by xw2sy on 2017-05-05.
 */
@Data
public class CartStockVo extends ProductStock {

    //在购物车中的数量
    private int cartAmount;
}
