package com.mgfireworks.mgplatform.mgshop.model;

import cn.hn.java.summer.utils.StringUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class ProductDetail extends Product{

	/**
	 * 图片列表 以,隔开
	 */
	private String images;
	
	/**
	 * 详情 
	 */
	private String detailContent;

	/**
	 * 获取图片列表
	 * @return
	 */
	public List<String> getImageList(){
		if(StringUtils.isBlank(images)){
			return new ArrayList<>();
		}
		return Arrays.asList(images.split(";"));
	}
	
}