package com.mgfireworks.mgplatform.mgshop.model.weixin;

import lombok.Data;

/**
 * Created by xw2sy on 2017-04-29.
 * 微信access_token
 */
@Data
public class WeXinAccessToken extends WeiXinResponse{
    //获取到的凭证
    private String access_token;
    //凭证有效时间，单位：秒
    private int expires_in;
}
