package com.mgfireworks.mgplatform.mgshop.model;

import com.mgfireworks.mgplatform.mgshop.db.ShopUser;
import lombok.Data;

/**
 * Created by xw2sy on 2017-04-29.
 */
@Data
public class ShopUserVo extends ShopUser {
}
