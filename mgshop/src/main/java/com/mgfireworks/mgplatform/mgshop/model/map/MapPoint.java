package com.mgfireworks.mgplatform.mgshop.model.map;

import lombok.Data;

/**
 * Created by xw2sy on 2017-04-30.
 * 地图经纬度坐标
 */
@Data
public class MapPoint {

    //经度
    private double lat;
    //纬度
    private double lng;

    public MapPoint(){}

    public MapPoint(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
}
