package com.mgfireworks.mgplatform.mgshop.model.map;

import lombok.Data;

/**
 * Created by xw2sy on 2017-04-30.
 */
@Data
public class MapApiConfig {

    private String ak;
}
