package com.mgfireworks.mgplatform.mgshop.model.weixin;

import lombok.Data;

/**
 * Created by xw2sy on 2017-04-29.
 */
@Data
public class WeiXinUserInfo extends WeiXinResponse {
    private java.lang.String openid;
    private java.lang.String nickname;
    private java.lang.String sex;
    private java.lang.String province;
    private java.lang.String city;
    private java.lang.String country;
    private java.lang.String headimgurl;
    private java.lang.String[] privilege;
    private java.lang.String unionid;
    private java.lang.String language;
}
