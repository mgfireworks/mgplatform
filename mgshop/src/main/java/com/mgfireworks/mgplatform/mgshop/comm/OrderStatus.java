package com.mgfireworks.mgplatform.mgshop.comm;

/**
 * 0 创建订单待定待支付 1 订单已支付待发货(-1退款) 2 订单已发货(-2取消发货) 3 订单已收货(-3退货) 4 订单完成
 */
public enum OrderStatus {
	NO_PAY(0,"待支付"),
	NO_DELIVERY(1,"待发货"),
	REFUND(-1,"退款"),
	DELIVERED(2,"已发货"),
	CANCEL_DELIVERY(-2,"已取消发货"),
	RECEIVED(3,"已收货"),
	RETURN(-3,"已退货"),
	FINISHED(4,"已完成"),
	CANCELED(-4,"已取消");

	private final int value;

	private final String phrase;

	private OrderStatus(int value, String phrase) {
		this.value = value;
		this.phrase = phrase;
	}
	/**
	 * @return 状态的整型编码
	 */
	public int value() {
		return this.value;
	}

	/**
	 * @return 状态的描述
	 */
	public String getPhrase() {
		return this.phrase;
	}


	public static OrderStatus valueOf(int v) {
		for (OrderStatus val : values()) {
			if (val.value == v) {
				return val;
			}
		}
		return null;
	}

	/**
	 * 返回状态的编号
	 */
	@Override
	public String toString() {
		return phrase;
	}
	
}
