package com.mgfireworks.mgplatform.mgshop.model;

import com.mgfireworks.mgplatform.mgshop.db.ProductStock;
import lombok.Data;

/**
 * Created by xw2sy on 2017-04-30.
 */
@Data
public class StockVo extends ProductStock {
}
