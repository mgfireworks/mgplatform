package com.mgfireworks.mgplatform.mgshop.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by summer.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopCart {
   private java.lang.Integer cartId;
   private java.lang.Integer orgId;
   private java.lang.Integer userId;
   private java.lang.String productId;
   private java.lang.String stockId;
   private java.lang.Integer amount;
   private java.math.BigDecimal price;
   private java.lang.Integer status;
   private java.util.Date createdTime;
   private java.util.Date updatedTime;
   private java.lang.String orderId;

}