package com.mgfireworks.mgplatform.mgshop.model;

import com.mgfireworks.mgplatform.mgshop.db.ShopUserInfo;
import com.mgfireworks.mgplatform.mgshop.model.map.MapPoint;
import lombok.Data;

/**
 * Created by xw2sy on 2017-04-30.
 */
@Data
public class ShopUserInfoVo extends ShopUserInfo {

    //店铺所属代理商id
    private Integer shopOrgId;

    private MapPoint point;

    public String getLatAndLng(){
        if(point==null || point.getLng()==0 || point.getLat()==0){
            return null;
        }
        return point.getLat()+","+point.getLng();
    }

    /**
     * 获取省市区全名
     * @return
     */
    public String getAreaFullName(){
        StringBuilder sb=new StringBuilder();
        sb.append(getProvince())
                .append(",")
                .append(getCity())
                .append(",")
                .append(getDistrict());
        return sb.toString();
    }
}
