package com.mgfireworks.mgplatform.mgshop.main;

import com.mgfireworks.mgplatform.BaseController;
import com.mgfireworks.mgplatform.mgshop.model.Product;
import com.mgfireworks.mgplatform.mgshop.model.ShopUserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by xw2sy on 2017-04-30.
 */
@Controller
@RequestMapping("/product")
public class ProductController extends BaseController{

    @Autowired
    private ProductService productService;

    @RequestMapping("/recommends")
    public List<Product> getRecommendProducts(){
        ShopUserInfoVo userInfoVo= getCurrentUser();
        return productService.getRecommendProducts(userInfoVo.getShopOrgId());
    }

}
