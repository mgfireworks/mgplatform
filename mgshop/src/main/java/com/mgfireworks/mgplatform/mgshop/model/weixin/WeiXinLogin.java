package com.mgfireworks.mgplatform.mgshop.model.weixin;

import lombok.Data;

/**
 * Created by xw2sy on 2017-04-29.
 */
@Data
public class WeiXinLogin extends  WeiXinResponse {

    private String code;

    private String state;
}
