package com.mgfireworks.mgplatform.mgshop.db;

import lombok.Data;

/**
 * Created by summer.
 */
@Data
public class ShopUser {
   private java.lang.Integer userId;
   private java.lang.String userAccount;
   private java.lang.String openid;
   private java.lang.String nickname;
   private java.lang.String sex;
   private java.lang.String headimgurl;
   private java.util.Date createTime;
   private java.util.Date loginTime;
   private java.lang.String loginIp;
   private java.lang.String loginCode;

}