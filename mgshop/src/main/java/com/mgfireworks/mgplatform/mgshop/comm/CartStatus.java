package com.mgfireworks.mgplatform.mgshop.comm;

/**
 * 购物车清单项状态
 */
public enum CartStatus {
	DISABLE(0,"移除"),
	ENABLE(1,"可用");

	private final int value;

	private final String phrase;

	private CartStatus(int value, String phrase) {
		this.value = value;
		this.phrase = phrase;
	}
	/**
	 * @return 状态的整型编码
	 */
	public int value() {
		return this.value;
	}

	/**
	 * @return 状态的描述
	 */
	public String getPhrase() {
		return this.phrase;
	}


	public static CartStatus valueOf(int v) {
		for (CartStatus val : values()) {
			if (val.value == v) {
				return val;
			}
		}
		return null;
	}

	/**
	 * 返回状态的编号
	 */
	@Override
	public String toString() {
		return phrase;
	}
	
}
