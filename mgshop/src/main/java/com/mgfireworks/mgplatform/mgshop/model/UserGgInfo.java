package com.mgfireworks.mgplatform.mgshop.model;

import lombok.Data;

/**
 * Created by xw2sy on 2017-04-30.
 */
@Data
public class UserGgInfo {

    private String province;
    private String city;
    private String district;
    private String street;
    private String streetNumber;

    //经度
    public double lat;
    //纬度
    public double lng;
}
