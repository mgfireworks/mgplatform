package com.mgfireworks.mgplatform.mgshop.db;

import lombok.Data;
import java.sql.*;
import java.math.*;

/**
 * Created by summer.
 */
@Data
public class Organization {
   private java.lang.Integer orgId;
   private java.lang.String orgName;
   private java.lang.Integer orgType;
   private java.lang.Integer parentId;
   private java.lang.Integer orgStatus;
   private java.lang.String orgAddress;
   private java.lang.String rangeMarks;
   private java.lang.String orgOfficial;
   private java.lang.String orgContact;
   private java.lang.String areaId;
   private java.lang.String areaFullName;
   private java.lang.String description;
   private java.lang.Integer bandId;

}