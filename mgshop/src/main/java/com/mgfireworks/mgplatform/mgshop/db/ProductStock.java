package com.mgfireworks.mgplatform.mgshop.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by summer.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductStock {
   private java.lang.String stockId;
   private java.lang.String nextStockId;
   private java.lang.Integer orgId;
   private java.lang.String productId;
   private java.lang.String areaId;
   private java.lang.Integer quantity;
   private java.math.BigDecimal price;
   private java.lang.Boolean online;
   private java.lang.Boolean isNew;
   private java.lang.Boolean outOfStock;
   private java.lang.String checkState;

}