package com.mgfireworks.mgplatform.mgshop.model.weixin;

import lombok.Data;

/**
 * Created by xw2sy on 2017-04-29.
 * app接入微信配置
 */
@Data
public class WeiXinAppConfig {

    private String appId;

    private String appsecret;

    private String token;

    private WeXinAccessToken weXinAccessToken;
}
