package com.mgfireworks.mgplatform.mgshop.db;

import lombok.Data;

/**
 * Created by summer.
 */
@Data
public class ShopUserInfo {
   private java.lang.Integer userId;
   private java.lang.String province;
   private java.lang.String city;
   private java.lang.String district;
   private java.lang.String street;
   private java.lang.String streetNumber;
   private java.lang.String receivingAddress;
   private java.lang.String latAndLng;

}