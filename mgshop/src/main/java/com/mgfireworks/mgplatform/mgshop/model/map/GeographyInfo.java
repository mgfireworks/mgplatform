package com.mgfireworks.mgplatform.mgshop.model.map;

import lombok.Data;

/**
 * Created by xw2sy on 2017-04-30.
 */
@Data
public class GeographyInfo {

    //返回结果状态值， 成功返回0，其他值请查看下方返回码状态表。
    private int status;
    //经纬度坐标
    private MapPoint location;
    //位置的附加信息，是否精确查找。1为精确查找，即准确打点；0为不精确，即模糊打点。
    private int precise;
    //可信度，描述打点准确度
    private int confidence;
    //地址类型
    private String level;
}
