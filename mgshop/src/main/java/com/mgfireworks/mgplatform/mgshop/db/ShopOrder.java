package com.mgfireworks.mgplatform.mgshop.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by summer.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopOrder {
   private java.lang.String orderId;
   private java.lang.Integer userId;
   private java.lang.Integer itemCount;
   private java.math.BigDecimal totalAmount;
   private java.lang.Integer status;
   private java.util.Date createdTime;
   private java.util.Date updatedTime;
   private java.lang.String updatedBy;

}