package com.mgfireworks.mgplatform.mgshop.model;

import lombok.Data;

/**
 * Created by xw2sy on 2017-05-03.
 * 购物车产品
 */
@Data
public class OrderItemDetail {

    private String productId;
    private String stockId;
    private String productName;
    private String mainImg;
    private Double price;
    private Integer amount;
}
