package main;

import cn.hn.java.summer.Config;
import cn.hn.java.summer.db.mapper.rule.IBeanMapperRule;
import cn.hn.java.summer.db.mapper.rule.UnderlinedBeanMapperRule;
import cn.hn.java.summer.web.WebApp;
import com.mgfireworks.mgplatform.ActionInterceptor;
import com.mgfireworks.mgplatform.mgshop.model.map.MapApiConfig;
import com.mgfireworks.mgplatform.mgshop.model.weixin.WeiXinAppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import java.io.IOException;
import java.net.URISyntaxException;

@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
@SpringBootApplication(scanBasePackages={"com.mgfireworks.mgplatform"})
@Configuration()
@EnableTransactionManagement
@EnableScheduling
@EnableCaching
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class App extends WebApp
{
    public static void main( String[] args ) throws URISyntaxException, IOException
    {
    	//支持设置restful风格请求的视图名
		Config.isIsSupportSetRestfulViewName =true;
        Config.dbBeanScanFilter=".*mgshop.*db";
    	SpringApplication.run(App.class);
    }

    @Bean
    public IBeanMapperRule getBeanMapperRule(){
        return new UnderlinedBeanMapperRule("mg_");
    }


    /**
     * 配置拦截器
     * @author lance
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(createActionInterceptor()).addPathPatterns("/**");
    }

    @Bean
    public ActionInterceptor createActionInterceptor(){
        return new ActionInterceptor();
    }


    /**
     * 微信api配置
     * @return
     */
    @Bean
    @ConfigurationProperties(prefix = "weixin.access")
    public WeiXinAppConfig getWeiXinAppConfig(){
        return new WeiXinAppConfig();
    }

    @Bean
    @ConfigurationProperties(prefix = "map.api.ak")
    public MapApiConfig getMapApiConfig(){
        return new MapApiConfig();
    }

}