CREATE TABLE `mg_shop_cart` (
  `cartId` int(11) NOT NULL AUTO_INCREMENT,
  `orgId` int(11) NOT NULL COMMENT '购物车id',
  `userId` int(11) NOT NULL COMMENT '用户id',
  `productId` varchar(12) DEFAULT NULL COMMENT '产品id',
  `stockId` varchar(14) DEFAULT NULL COMMENT '库存编号',
  `amount` int(4) NOT NULL COMMENT '产品数量',
  `price` decimal(10,2) NOT NULL COMMENT '产品单价',
  `status` int(11) NOT NULL COMMENT '0 移除购物车 1 加入购物车',
  `createdTime` datetime NOT NULL COMMENT '创建时间',
  `updatedTime` datetime NOT NULL COMMENT '更新时间',
  `orderId` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cartId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='购物车表';
