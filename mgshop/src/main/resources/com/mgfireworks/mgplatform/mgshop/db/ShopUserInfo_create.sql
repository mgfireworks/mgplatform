CREATE TABLE `mg_shop_user_info` (
  `userId` int(11) NOT NULL,
  `province` varchar(15) DEFAULT NULL COMMENT '省',
  `city` varchar(30) DEFAULT NULL COMMENT '市',
  `district` varchar(45) DEFAULT NULL COMMENT '区、县',
  `street` varchar(45) DEFAULT NULL COMMENT '街道',
  `streetNumber` varchar(45) DEFAULT NULL COMMENT '牌号',
  `receivingAddress` varchar(200) DEFAULT NULL,
  `latAndLng` varchar(45) DEFAULT NULL COMMENT '用户地址所在的经纬度',
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='店铺用户信息表';
