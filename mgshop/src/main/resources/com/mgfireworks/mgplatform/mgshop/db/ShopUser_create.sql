CREATE TABLE `mg_shop_user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userAccount` varchar(45) NOT NULL COMMENT '用户账号',
  `openid` varchar(45) DEFAULT NULL COMMENT '微信用户id',
  `nickname` varchar(45) DEFAULT NULL COMMENT '昵称',
  `sex` varchar(1) DEFAULT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `headimgurl` varchar(150) DEFAULT NULL COMMENT '用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。',
  `createTime` datetime DEFAULT NULL,
  `loginTime` datetime DEFAULT NULL,
  `loginIp` varchar(45) DEFAULT NULL,
  `loginCode` varchar(45) DEFAULT NULL COMMENT '微信登录code',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='店铺用户表';
