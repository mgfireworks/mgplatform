CREATE TABLE `mg_shop_order` (
  `orderId` varchar(20) NOT NULL COMMENT '订单号(5位orgId +7位 uid  +2位天  +2位时 +2位分 mm+2位秒 ss)',
  `userId` int(11) NOT NULL COMMENT '用户id',
  `itemCount` int(11) NOT NULL,
  `totalAmount` decimal(12,2) NOT NULL COMMENT '订单总额',
  `status` int(11) NOT NULL COMMENT '0 创建订单待定待支付 1 订单已支付待发货(-1退款) 2 订单已发货(-2取消发货) 3 订单已收货(-3退货) 4 订单完成',
  `createdTime` datetime NOT NULL COMMENT '创建时间(下单时间)',
  `updatedTime` datetime NOT NULL COMMENT '更新时间(订单状态更改)',
  `updatedBy` varchar(30) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`orderId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='订单信息表';
