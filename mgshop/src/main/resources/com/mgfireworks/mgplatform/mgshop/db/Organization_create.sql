CREATE TABLE `mg_organization` (
  `orgId` int(11) NOT NULL COMMENT '机构编号 ',
  `orgName` varchar(100) NOT NULL COMMENT '机构名称',
  `orgType` int(11) DEFAULT NULL COMMENT '机构级别(0:平台,1:厂家,2:一级代理商,3:二级代理商)',
  `parentId` int(11) DEFAULT NULL COMMENT '父级机构',
  `orgStatus` int(11) NOT NULL DEFAULT '0' COMMENT '状态(0:未审核,1:已审核)',
  `orgAddress` varchar(150) DEFAULT NULL COMMENT '机构地址',
  `rangeMarks` varchar(500) DEFAULT NULL COMMENT '代理商经营范围标记',
  `orgOfficial` varchar(20) DEFAULT NULL COMMENT '负责人',
  `orgContact` varchar(100) DEFAULT NULL COMMENT '联系方式',
  `areaId` varchar(12) NOT NULL COMMENT '区域编号',
  `areaFullName` varchar(100) DEFAULT NULL COMMENT '精确到区县的区域全名，如：xx省xx市xx县/区',
  `description` varchar(255) DEFAULT NULL COMMENT '说明',
  `bandId` int(11) DEFAULT NULL COMMENT '当机构为厂家时，需要绑定品牌编号',
  PRIMARY KEY (`orgId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构(工厂、公司等机构)';
