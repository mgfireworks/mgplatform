CREATE TABLE `mg_product_stock` (
  `stockId` varchar(14) NOT NULL COMMENT '库存编号(14位：4位机构编号+yyMMdd+4位序号)',
  `nextStockId` varchar(14) DEFAULT NULL COMMENT '下一个库存编号',
  `orgId` int(11) NOT NULL COMMENT '所属机构编号',
  `productId` varchar(12) NOT NULL COMMENT '产品编号',
  `areaId` varchar(12) NOT NULL COMMENT '区域编号',
  `quantity` int(11) NOT NULL DEFAULT '0' COMMENT '库存数',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `online` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否上线(0:未上线,1:上线)',
  `isNew` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否最新上架(0:不是最新,1:最新)',
  `outOfStock` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否缺货(0:缺,1:不缺)',
  `checkState` varchar(255) DEFAULT NULL COMMENT '审核状态',
  PRIMARY KEY (`stockId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品仓库(各级分销机构的产品仓库)';
